---
title: "Soil parameter framework 2021 - Step 1 spatial framework"
author: "Tessa Chamberlain"
date: "March 2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(rgdal)
library(sf)
library(DBI)
library(ROracle)

rm(list=ls(all=TRUE))

```

``` {r Oracle connections, echo=FALSE, eval=TRUE}

drv    <- dbDriver('Oracle')

# SALI Connection
SALI <- dbConnect(drv,
                  username = 'chamberlaint',
                  password = 'October31!',
                  dbname = 'saliprod',
                  host = 'lasun412',
                  port = 1521)

```

This script contains some spatial processing steps that were required to develop a new spatial layer of SPCs in GBR grain cropping, banana, and cane areas. Many other steps were carried out in ArcGIS. All spatial processing steps are described in a spreadsheet *draft_soil_framework_2021_spatial_processing_steps.xlsx*.

## Note - BASP layer

Up til August 2022 I have used an early version of the BASP layer, from February 2021. This was used to generate SPC lists for all coastal cropping areas (CYWT, CB, MW, CBM). In September 2022 I received an updated layer from Kaitlyn Andrews, which has the CQCS mapping included as well as other minor edits. This layer will be used to generate SPC lists for the inland grains areas (FIB, IB). 


## Stage 1 - cleaning up Peter's land unit mapping 

In ArcGIS I worked on the best available spatial polygon (BASP) layer and also cleaned Peter Zund's disaggreated land unit mapping

## Stage 2 - Create crop footprints

Here I extract cropping areas from the catchment modellers' FU layers and combine them to provide a new crop footprint.

```{r import FU layers}

crop_CY <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'ExportedCatchments-FUs Intersection') %>%
        filter(., IntFUs %in% c("Bananas", "Dryland Cropping", "Irrigated Cropping", "Horticulture")) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Cape York")
crop_WT <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'wt_subcatch_FU_intersect') %>%
        filter(., IntFUs %in% c("Bananas", "Dryland Cropping", "Irrigated Cropping", "Sugarcane", "Horticulture")) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Wet Tropics")
crop_BU <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'BU_IntSCFU2019') %>%
        filter(., IntFUs %in% c("Dryland Cropping", "Irrigated Cropping", "Sugarcane", "Horticulture")) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Burdekin")
crop_MW <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'MW_IntSCFU2018') %>%
        filter(., IntFUs %in% c("Irrigated Cropping", "Sugarcane", "Horticulture")) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Mackay-Whitsunday")
crop_FZ <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'FI_RC2019_Intsct_Subcat_FUs') %>%
        filter(., IntFU %in% c("Dryland Cropping", "Irrigated Cropping", "Sugarcane", "Horticulture")) %>%
        rename(., IntFUs = IntFU, IntSCs = IntSC) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Fitzroy")
crop_BM <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'BM_IntSCFU_2019') %>%
        filter(., IntFUs %in% c("Dryland Cropping", "Irrigated Cropping", "Sugarcane", "Horticulture")) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Burnett-Mary")

crop_all <- bind_rows(crop_CY, crop_WT, crop_BU, crop_MW, crop_FZ, crop_BM)

st_write(crop_CY, "./1_spatial_framework/spatial_data/FU_layers/crop_CY.shp")
st_write(crop_WT, "./1_spatial_framework/spatial_data/FU_layers/crop_WT.shp")
st_write(crop_BU, "./1_spatial_framework/spatial_data/FU_layers/crop_BU.shp")
st_write(crop_MW, "./1_spatial_framework/spatial_data/FU_layers/crop_MW.shp")
st_write(crop_FZ, "./1_spatial_framework/spatial_data/FU_layers/crop_FZ.shp")
st_write(crop_BM, "./1_spatial_framework/spatial_data/FU_layers/crop_BM.shp")
st_write(crop_all, "./1_spatial_framework/spatial_data/FU_layers/crop_all.shp")

# bring in all FUs to calc GBR wide areas for presentation
LU_CY <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'ExportedCatchments-FUs Intersection') %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Cape York")
LU_WT <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'wt_subcatch_FU_intersect') %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Wet Tropics")
LU_BU <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'BU_IntSCFU2019') %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Burdekin")
LU_MW <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'MW_IntSCFU2018') %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Mackay-Whitsunday")
LU_FZ <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'FI_RC2019_Intsct_Subcat_FUs') %>%
        rename(., IntFUs = IntFU, IntSCs = IntSC) %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Fitzroy")
LU_BM <- st_read(dsn = './1_spatial_framework/spatial_data/FU_layers', layer = 'BM_IntSCFU_2019') %>%
        select(., IntFUs, IntSCs) %>%
        mutate(., Region = "Burnett-Mary")

LU_all <- bind_rows(LU_CY, LU_WT, LU_BU, LU_MW, LU_FZ, LU_BM)

LU_all$Area_m2 <- st_area(LU_all)
LU_all$Area_ha <- LU_all$Area_m2 / 10000

st_write(LU_all, "./1_spatial_framework/spatial_data/FU_layers/LU_all.shp")

FU_summary_wholeGBR <- aggregate(Area_ha ~  IntFUs, FUN = sum, data = LU_all, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

write.csv(FU_summary_wholeGBR, "./1_spatial_framework/FU_summary_wholeGBR.csv")

library(tmap)
qtm(LU_all)

```

Just do a quick area summary of FU types in each region. Particularly to look at the area of horticulture, is it small enough that I can add into the analysis without increasing my workload too much? From this I think yes, I will include it. It only increases the crop footprint by 5%, and from a quick look it mostly falls within good SPC mapping.

```{r}

crop_all$Area_ha <- st_area(crop_all) / 10000

FU_region_summary <- aggregate(Area_ha ~  Region + IntFUs, FUN = sum, data = crop_all, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., Region, desc(Area_ha))

total_crop_area <- group_by(FU_region_summary) %>%
  summarise(., Area_ha = sum(as.numeric(Area_ha)))

FU_summary <- aggregate(Area_ha ~ IntFUs, FUN = sum, data = crop_all, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

write.csv(FU_region_summary, "./1_spatial_framework/FU_region_summary.csv")
write.csv(total_crop_area, "./1_spatial_framework/total_crop_area.csv")
write.csv(FU_summary, "./1_spatial_framework/FU_summary.csv")

```


## Stage 3 - Land unit processing - step 3b - create list of first/best SPC for Gunn & Nix land units

Here I am pulling the first SPC record out of my Gunn and Nix land unit look up table (transcribed from their report for the 2018 framework), and joining it to the clipped CQ land unit layers.

```{r Gunn and Nix}

gunn_nix <- read.csv("./1_spatial_framework/gunn_nix_LU_SPC_links.csv", header=T)

gunn_nix_1 <- filter(gunn_nix, Component_no == 1) %>%
  select(., LU_number, Name)

write.csv(gunn_nix_1, "./1_spatial_framework/gunn_nix_LU_SPC_links_1.csv")

```

Now summarising area of SPCs in each of the three surveys, to check whether I'm still getting lots of 'wrong' soils occurring in cropping areas.

```{r check PZ soils in cropping areas}

# import layers
zcq_SPC1 <- st_read(dsn = "./1_spatial_framework/spatial_data/PZ_ZCQ_processing.gdb", layer = "zcq_AA_E3S_BASP_C_SPC1")
zdk_SPC1 <- st_read(dsn = "./1_spatial_framework/spatial_data/PZ_ZDK_processing.gdb", layer = "zdk_AA_E3S_BASP_C_SPC1")
zdd_SPC1 <- st_read(dsn = "./1_spatial_framework/spatial_data/PZ_ZDD_processing.gdb", layer = "zdd_AA_E3S_BASP_C_SPC1")

# summarise LU areas
zcq_LU_areas <- aggregate(Area_ha ~  LU_number, FUN = sum, data = zcq_SPC1, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

zdk_LU_areas <- aggregate(Area_ha ~  LU_number, FUN = sum, data = zdk_SPC1, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

zdd_LU_areas <- aggregate(Area_ha ~  LU_number, FUN = sum, data = zdd_SPC1, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

# summarise SPC 1 areas
zcq_SPC1_areas <- aggregate(Area_ha ~ Name, FUN = sum, data = zcq_SPC1, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

zdk_SPC1_areas <- aggregate(Area_ha ~ Name, FUN = sum, data = zdk_SPC1, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

zdd_SPC1_areas <- aggregate(Area_ha ~ Name, FUN = sum, data = zdd_SPC1, na.rm = TRUE) %>%
  mutate(., Area_ha = round(Area_ha)) %>%
  arrange(., desc(Area_ha))

write.csv(zcq_SPC1_areas, "./1_spatial_framework/zcq_SPC1_areas.csv")
write.csv(zdk_SPC1_areas, "./1_spatial_framework/zdk_SPC1_areas.csv")
write.csv(zdd_SPC1_areas, "./1_spatial_framework/zdd_SPC1_areas.csv")

```

RESULT - from this I have found that there are significant areas of Sodosols (e.g. Retro) and shallow rock soils (e.g. Bruce) that are mapped in the cropping areas. These are very likely incorrect. I'm now thinking that I will instead use the old land system polygons, and will extract the highest capability land unit in each to assign SPCs. With enough sliver removal I think this may be a better option than Peter's disaggregated land units as I won't get too much weird stuff that really doesn't belong in cropping.

## Stage 4 - clean up BASP layer

In ArcGIS, I have done some Eliminates to remove small polygons and slivers.

## Stage 5 - Attach first entity to each BASP poygon - steps 5a to 5c

Here I need to get the linkage between polygons and their first (dominant) entity. Also need their entity name and entity no (i.e. SPC no or land comp no) to hang other data off later. Kaitlyn has a map code in her BASP layer but they aren't the actual entity codes (e.g. Oxford has a mapcode of O but an entity code of Ox).

In ArcGIS I join the resultant UMA > entity table to the BASP layer, to get entity codes attached to every polygon.

For SPC-based mapping, this provides the final SPC codes. Land system and other surveys will be handled separately in following steps.

```{r polygon entity codes}

# get list of all UMA entity 1's, and their proj_polys to link to spatial data
UMA_ENTITY1 <- dbGetQuery(SALI, "select
                                   project_code, 
                                   poly_no,
                                   project_code || '_' || poly_no as proj_poly,
                                   entity_type,
                                   entity_code
                                   from
                                   uma_entities
                                   where
                                   entity_no = 1
                                   ")

write.csv(UMA_ENTITY1, "./1_spatial_framework/UMA_ENTITY1.csv")

```

###For the moment, I'm doing area summaries just including SPC entities from finer scale mapping, not from land system mapping. That is a start and I can come back to the land system issue later.

```{r area summaries}

# Areas of various entity types
BASP_SPCs <- st_read(dsn = './1_spatial_framework/spatial_data/QSRIS_layer_drafts.gdb', layer = 'BASP_polygons_draft_C_SP_E3_ENT_U')

### Sept 2021 - getting proj/entity/spc_no/spc name link to join to shapefile
proj_ent1_link <- dbGetQuery(SALI, "select
                             project_code, 
                             poly_no,
                             entity_no,
                             entity_type,
                             entity_code
                             from
                             uma_entities
                             where
                             entity_no = 1")

ent_spc_link <- dbGetQuery(SALI, "select
                           s.project_code,
                           s.label,
                           s.spc_no,
                           a.name
                           from
                           spc_attrib_projects s
                           left join
                           spc_attributes a
                           on
                           s.spc_no = a.spc_no")

poly_spc_link <- filter(proj_ent1_link, ENTITY_TYPE == "SPC") %>%
                 left_join(., ent_spc_link, by = c("PROJECT_CODE", "ENTITY_CODE" = "LABEL")) %>%
                 mutate(., proj_poly = paste(PROJECT_CODE, POLY_NO, sep = "_"))
write.csv(poly_spc_link, "./1_spatial_framework/poly_spc_link.csv") ##### This table is linked back to the shapefile in ArcMap #####

Area_entity_types <- aggregate(Area_ha ~ ENTITY_TxYPE, FUN = sum, data = BASP_SPCs, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., desc(Area_ha))

write.csv(Area_entity_types, "./1_spatial_framework/Area_entity_types.csv")

# Areas of entity types in each region
Area_entity_types_NRMregion <- aggregate(Area_ha ~ Region + ENTITY_TYPE, FUN = sum, data = BASP_SPCs, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Region, desc(Area_ha))

write.csv(Area_entity_types_NRMregion, "./1_spatial_framework/Area_entity_types_NRMregion.csv")

# Also by FU
Area_entity_types_NRMregion_FU <- aggregate(Area_ha ~ Region + IntFUs + ENTITY_TYPE, FUN = sum, data = BASP_SPCs, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Region, IntFUs, desc(Area_ha))

write.csv(Area_entity_types_NRMregion_FU, "./1_spatial_framework/Area_entity_types_NRMregion_FU.csv")

# Just by entity and FU
Area_entity_types_FU <- aggregate(Area_ha ~ IntFUs + ENTITY_TYPE, FUN = sum, data = BASP_SPCs, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., IntFUs, desc(Area_ha))

write.csv(Area_entity_types_FU, "./1_spatial_framework/Area_entity_types_FU.csv")

# Areas of entity types in each cluster region
Area_entity_types_Clustregion <- aggregate(Area_ha ~ Clust_reg + ENTITY_TYP, FUN = sum, data = BASP_SPCs_NAME, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Clust_reg, desc(Area_ha))

write.csv(Area_entity_types_Clustregion, "./1_spatial_framework/Area_entity_types_Clustregion.csv")

# Areas of each cluster region
Area_Clustregion <- aggregate(Area_ha ~ Cluster_reg, FUN = sum, data = BASP_SPCs, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Cluster_reg, desc(Area_ha))

write.csv(Area_Clustregion, "./1_spatial_framework/Area_Clustregion.csv")

## May 2021 - areas of entity types in TULLY catchment, to use as a pilot area
BASP_SPCs_Tully <- st_read(dsn = './1_spatial_framework/spatial_data/QSRIS_layer_drafts.gdb', layer = 'BASP_polygons_draft_C_SP_E3_ENT_U_Tully')

Area_entity_types_Tully <- aggregate(Area_ha ~ ENTITY_TYPE, FUN = sum, data = BASP_SPCs_Tully, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., desc(Area_ha))

Area_entity_types_FU_Tully <- aggregate(Area_ha ~ IntFUs + ENTITY_TYPE, FUN = sum, data = BASP_SPCs_Tully, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., IntFUs, desc(Area_ha))

write.csv(Area_entity_types_Tully, "./1_spatial_framework/Area_entity_types_Tully.csv")
write.csv(Area_entity_types_FU_Tully, "./1_spatial_framework/Area_entity_types_FU_Tully.csv")

## May 2021 - areas of entity types in MACKAY WHITSUNDAY region, to use as a pilot area
BASP_SPCs_MW <- st_read(dsn = './1_spatial_framework/spatial_data/QSRIS_layer_drafts.gdb', layer = 'BASP_polygons_draft_C_SP_E3_ENT_U_LBMW') %>%
                  filter(., Cluster_reg == "Mackay-Whitsunday")

Area_entity_types_MW <- aggregate(Area_ha ~ Cluster_reg + ENTITY_TYPE, FUN = sum, data = BASP_SPCs_MW, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Cluster_reg, desc(Area_ha))

Area_entity_types_FU_MW <- aggregate(Area_ha ~ Cluster_reg + IntFUs + ENTITY_TYPE, FUN = sum, data = BASP_SPCs_MW, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Cluster_reg, IntFUs, desc(Area_ha))

write.csv(Area_entity_types_MW, "./1_spatial_framework/Area_entity_types_MW.csv")
write.csv(Area_entity_types_FU_MW, "./1_spatial_framework/Area_entity_types_FU_MW.csv")

```

Now I will generate a list of SPCs that occur in each region. I need to remove the project code, entity code and entity number (the SPC number) as they create duplicates. Just want a list of the actual SPC names, so I can see exactly how many soils are going into the clustering.

I will also make a second table, with the project codes and entity codes retained, so that I can link up site data for the clustering in the next stage.

```{r}

BASP_SPCs_NAME <- st_read(dsn = './1_spatial_framework/spatial_data', layer = 'BASP_polygons_draft_C_SP_E3_ENT_U_NAMES_Creg')

BASP_SPCs_NAME$SPC_NO <- as.integer(BASP_SPCs_NAME$SPC_NO)

##############
# summarise ASCs in different land uses
# Add in ASC and PPF from SALI
SALI_SPC_ASC1 <- dbGetQuery(SALI, "select 
                                  at.spc_no, 
                                  at.name,
                                  a.asc_no, 
                                  a.asc_ord, 
                                  a.subord_asc_code as subord
                                  from 
                                  spc_attributes at
                                  left join
                                  spc_attrib_asc a
                                  on 
                                  at.spc_no = a.spc_no
                                  where 
                                  a.asc_no = 1
                                  ")

BASP_SPCs_NAME <- left_join(BASP_SPCs_NAME, SALI_SPC_ASC1, by = c("SPC_NO_1" = "SPC_NO", "NAME"))

ASC_summary <- aggregate(Area_ha ~ IntFUs + ASC_ORD, FUN = sum, data = BASP_SPCs_NAME) %>%                      
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., IntFUs, desc(Area_ha))

write.csv(ASC_summary, "./1_spatial_framework/ASC_summary.csv")
#############

SPC_names <- aggregate(Area_ha ~ Clust_reg + ENTITY_TYP + NAME, FUN = sum, data = BASP_SPCs_NAME, na.rm = TRUE) %>%
                        filter(., ENTITY_TYP == "SPC") %>%                      
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Clust_reg, desc(Area_ha))


SPC_names_CountDup <- group_by(SPC_names, NAME) %>%
  summarise(., count=n()) %>%
  arrange(., desc(count))
# this shows that a few SPCs occur across two regions - I guess that's ok, they can get clustered twice

write.csv(SPC_names, "./1_spatial_framework/SPC_names.csv")

# count how many SPCs are recorded in each region
SPC_names_regioncount <- group_by(SPC_names, Clust_reg) %>%
  summarise(., count=n())

write.csv(SPC_names_regioncount, "./1_spatial_framework/SPC_names_regioncount.csv")

# also generate list of SPCs with their names and SPC numbers, so I can link up site data in next step via tax unit codes
SPC_names_ent_nos <- aggregate(Area_ha ~ Clust_reg + ENTITY_TYP + NAME + SPC_NO_1, FUN = sum, data = BASP_SPCs_NAME, na.rm = TRUE) %>%
                        filter(., ENTITY_TYP == "SPC") %>%                      
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Clust_reg, NAME, desc(Area_ha))

SPC_names_ent_nos_MW <- aggregate(Area_ha ~ Clust_reg + ENTITY_TYPE + NAME + ENTITY_NO, FUN = sum, data = BASP_SPCs_MW, na.rm = TRUE) %>%
                        filter(., ENTITY_TYPE == "SPC") %>%                      
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Clust_regg, NAME, desc(Area_ha))

write.csv(SPC_names_ent_nos, "./1_spatial_framework/SPC_names_ent_nos.csv")
write.csv(SPC_names_ent_nos_MW, "./1_spatial_framework/SPC_names_ent_nos_MW.csv")

## July 2022 - summarise list of projects in each clustering region
# this is so I can easily assess differences in limitations or other data 
proj_regions <- aggregate(Area_ha ~ Clust_reg + PROJECT_CO, FUN = sum, data = BASP_SPCs_NAME, na.rm = TRUE) %>%
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Clust_reg, PROJECT_CO, desc(Area_ha))

write.csv(proj_regions, "./1_spatial_framework/proj_regions.csv", row.names=F)

```

## April 2022 - Burdekin cane soils list

Also generating a list of only sugarcane SPCs in the Coastal Burdekin area - as I'm going to limit the clustering in that area to sugarcane only. This is the most complex region (most SPCs) and I have decided with Nev to cut the list back to only cane soils so it is easier for him to review the groupings.

```{r}

SPC_names_FUs <- aggregate(Area_ha ~ Clust_reg + IntFUs + ENTITY_TYP + NAME, FUN = sum, data = BASP_SPCs_NAME, na.rm = TRUE) %>%
                        filter(., ENTITY_TYP == "SPC") %>%                      
                        mutate(., Area_ha = round(Area_ha)) %>%
                        arrange(., Clust_reg, IntFUs, desc(Area_ha))

SPC_names_cane_CB <- filter(SPC_names_FUs, Clust_reg == "Coastal Burdekin" & IntFUs == "Sugarcane")
write.csv(SPC_names_cane_CB, "./1_spatial_framework/SPC_names_cane_CB.csv", row.names=F)

```

## June 2022 - adding list of UMA limitation codes where they may be useful for clustering in the Burdekin cane lands

Nev has suggested that sodicity and water availability limitations may be useful for the clustering. Having a look at them here.

```{r}

UMA_ENTITY_LIMS <- dbGetQuery(SALI, "select
                              project_code,
                              poly_no,
                              entity_no,
                              lim_code,
                              lim_value
                              from
                              uma_entity_limitations
                              where
                              lim_code in ('M', 'SU', 'Su', 'S')
                              ") %>%
                    filter(!(LIM_VALUE %in% c("-", "B0", "B1", "B2", "B3"))) ##remove dashes that represent no data, and a couple of salinity codes

# limit to just Coastal Burdekin projects
UMA_ENTITY_LIMS_CB <- left_join(filter(proj_regions, Clust_reg == "Coastal Burdekin"), UMA_ENTITY_LIMS, by = c("PROJECT_CO" = "PROJECT_CODE")) %>% select(-Area_ha)

# convert S codes in Burdekin to match Su codes - I have a LUT where I have updated codes for each project
CB_S_LimCode_LUT <- read.csv("./1_spatial_framework/CB_S_LimCode_LUT.csv", header=T)
CB_S_LimCode_LUT$new_Su <- as.character(CB_S_LimCode_LUT$new_Su)

UMA_ENTITY_LIMS_CB <- left_join(UMA_ENTITY_LIMS_CB, CB_S_LimCode_LUT, by = c("PROJECT_CO" = "PROJECT_CODE", "LIM_CODE", "LIM_VALUE")) %>%
                      mutate(LIM_VALUE = if_else(is.na(new_Su), LIM_VALUE, new_Su),   ##replace old S values with my Su values
                             LIM_VALUE = if_else(LIM_VALUE == "SD2", "2", LIM_VALUE), ##fix one odd code in S, 
                             LIM_VALUE = case_when(LIM_VALUE == "M1" ~ "0",
                                                   LIM_VALUE == "M2" ~ "0",
                                                   LIM_VALUE == "M3" ~ "1",
                                                   LIM_VALUE == "M4" ~ "2", 
                                                   LIM_VALUE == "M5" ~ "2",
                                                   LIM_VALUE == "M6" ~ "3",
                                                   TRUE ~ LIM_VALUE), ## M codes in WCS project
                             LIM_CODE = if_else(LIM_CODE == "S", "Su", LIM_CODE)) %>%   ##replace S code with Su code to combine
                      select(-c("DESCRIPTION", "new_Su"))


# bring poly-SPC link table back in
poly_spc_link <- read.csv("./1_spatial_framework/poly_spc_link.csv", header=T) %>% select(-X)

# join lim data to polys and their SPCs
UMA_LIMS_SPCs <- inner_join(UMA_ENTITY_LIMS_CB, poly_spc_link, by = c("PROJECT_CO" = "PROJECT_CODE", "POLY_NO", "ENTITY_NO"))

SPCs_lim_counts <- group_by(UMA_LIMS_SPCs, NAME, LIM_CODE, LIM_VALUE) %>% tally() %>% ungroup()

SPCs_lim_dom <- group_by(SPCs_lim_counts, NAME, LIM_CODE) %>% slice(which.max(n)) %>%
                pivot_wider(., id_cols = NAME, names_from = LIM_CODE, values_from = LIM_VALUE) %>%
                ungroup()

# limit to just cane soils in Burdekin
SPC_names_cane_CB <- read.csv("./1_spatial_framework/SPC_names_cane_CB.csv", header=T)

SPCs_lim_dom_CBcane <- left_join(select(SPC_names_cane_CB, NAME), SPCs_lim_dom, by = "NAME") %>%
                       arrange(NAME)

# Two SPCs have different M codes - change to match other surveys
SPCs_lim_dom_CBcane <- mutate(SPCs_lim_dom_CBcane, M = case_when(NAME == "Pennsfield" ~ "2", 
                                                                 NAME == "Carew" ~ "0",
                                                                 TRUE ~ M))

write.csv(SPCs_lim_dom_CBcane, "./1_spatial_framework/SPCs_lim_dom_CBcane.csv", row.names=F)

```


## Sept 2022 - summarising draft soils in banana areas

```{r}

Soils_Nanas_CYWT <- st_read(dsn = './1_spatial_framework/spatial_data', layer = 'Soil_gps_CYWT_E_Filled_D_FUs_Nanas')

Soils_Nanas_CYWT$Area_ha <- st_area(Soils_Nanas_CYWT)/ 10000

Area_Soils_Nanas <- aggregate(Area_ha ~ soil_grp, FUN = sum, data = Soils_Nanas_CYWT, na.rm = TRUE) %>%
                    mutate(., Area_ha = round(Area_ha)) %>%
                    arrange(., desc(Area_ha))

write.csv(Area_Soils_Nanas, "./1_spatial_framework/Area_Soils_Nanas.csv", row.names=F)

```

## Sept 2022 - get soil lists for inland grains areas

For these areas, I am using a newer BASP polygon layer received from Kaitlyn Andrews. I have clipped and edited it in the same manner as the earlier layer, and limited to only the Fitzroy/inland Burdekin and inland Burnett regions. These areas will be handled differently to the other regions due to the dominance of land system mapping.

First I will summarise the types of mapping, based on the entity type attribute Kaitlyn has included with the layer, and the project codes.

There are 38 projects covering the inland grains areas, the main ones being - CQCS, ZDD3, KCM, ZDK3, SBT, ZCQ2, ABS, BAN, WDH, EIL, ABC, 3MC, LFZ, EIR, NCD, CCL3, IPB, ABN. These are a mix of land unit, land system, and SPC mapping. CQCS is nominally a 250K land unit map, but it has been attributed with SPC entities so I will use those. 

Land system surveys will have an SPC attributed by determining the highest capability land unit, and using its dominant (first) SPC. 

# Summary of mapping types and projects

```{r}

BASP_grains <- st_read(dsn = './1_spatial_framework/spatial_data/QSRIS_POLYS_DRAFT_300822.gdb', layer = 'BASP_C_SP_E3_ClustReg_FIB_IB')

Area_MapTypes <- aggregate(AREA_HA ~ DOMINANT_ENTITY_TYPE, FUN = sum, data = BASP_grains, na.rm = TRUE) %>%
                 mutate(., AREA_HA = round(AREA_HA)) %>%
                 arrange(., desc(AREA_HA))

write.csv(Area_MapTypes, "./1_spatial_framework/Area_MapTypes_FIB_IB.csv", row.names=F)

Area_ProjCodes <- aggregate(AREA_HA ~ PROJECT_CODE + DOMINANT_ENTITY_TYPE, FUN = sum, data = BASP_grains, na.rm = TRUE) %>%
                 mutate(., AREA_HA = round(AREA_HA)) %>%
                 arrange(., desc(AREA_HA))

write.csv(Area_ProjCodes, "./1_spatial_framework/Area_ProjCodes_FIB_IB.csv", row.names=F)

## NOv 2022 - also calculating summary of entity types and projects for all regions with new version of BASP
BASP_new <- st_read(dsn = './1_spatial_framework/spatial_data/QSRIS_POLYS_DRAFT_300822.gdb', layer = 'BASP_C_SP_E3_I')

Area_MapTypes_allNew <- aggregate(AREA_HA ~ DOMINANT_ENTITY_TYPE, FUN = sum, data = BASP_new, na.rm = TRUE) %>%
                 mutate(., AREA_HA = round(AREA_HA)) %>%
                 arrange(., desc(AREA_HA))

write.csv(Area_MapTypes_allNew, "./1_spatial_framework/Area_MapTypes_allNew.csv", row.names=F)

Area_MapTypes_NRMRegions_allNew <- aggregate(AREA_HA ~  NRMREG + DOMINANT_ENTITY_TYPE, FUN = sum, data = BASP_new, na.rm = TRUE) %>%
                 mutate(., AREA_HA = round(AREA_HA)) %>%
                 arrange(., desc(AREA_HA))

write.csv(Area_MapTypes_NRMRegions_allNew, "./1_spatial_framework/Area_MapTypes_NRMRegions_allNew.csv", row.names=F)

Area_ProjCodes_allNew <- aggregate(AREA_HA ~ PROJECT_CODE + DOMINANT_ENTITY_TYPE, FUN = sum, data = BASP_new, na.rm = TRUE) %>%
                 mutate(., AREA_HA = round(AREA_HA)) %>%
                 arrange(., desc(AREA_HA))

write.csv(Area_ProjCodes_allNew, "./1_spatial_framework/Area_ProjCodes_allNew.csv", row.names=F)

```
 
# SPC-based mapping: pull out SPC list and set aside

The majority of mapping in the grains regions is SPC based. For these survey types, I link the SPC NAME to polygons via the dominant entities (defined on line 271 for other SPC mapping above) and then summarise an SPC list

```{r}

# link SPC NAME
BASP_grains <- left_join(BASP_grains, poly_spc_link, by = c("PROJECT_CODE", "POLY_NO", "PROJ_POLY" = "proj_poly"))

SPC_names1_FIB_IB <- aggregate(AREA_HA ~ Clust_reg + NAME + ENTITY_TYPE, FUN = sum, data = BASP_grains, na.rm = TRUE) %>%
                    filter(., ENTITY_TYPE == "SPC") %>%  
                    mutate(., Area_ha = round(AREA_HA)) %>%
                    arrange(., Clust_reg, desc(Area_ha)) %>%
                    select(., -c(ENTITY_TYPE, AREA_HA))

# also generate list of SPCs with their names and SPC numbers, so I can link up site data in next step via tax unit codes
SPC_names1_ent_nos_FIB_IB <- aggregate(AREA_HA ~ Clust_reg + ENTITY_TYPE + NAME + SPC_NO, FUN = sum, data = BASP_grains, na.rm = TRUE) %>%
                        filter(., ENTITY_TYPE == "SPC") %>%                      
                        mutate(., Area_ha = round(AREA_HA)) %>%
                        arrange(., Clust_reg, NAME, desc(Area_ha)) %>%
                        select(., -c(ENTITY_TYPE, AREA_HA))

```

# Land system mapping: define 'best' land units and SPCs

For land system surveys I will pull out the linkages between LS > best LU. There are land capability classes recorded in SALI for land units, which I should be able to use to determine the best LU in each system. However these are all incorrect, it looks to be an issue where these are automatically generated in SALI when individual LU limitations are entered. *So I will manually pull out the individual limitation records for each LU, and take the higher number to get a capability class for each LU (this is how the automatic process should have been working).*

If there are multiple LUs in a LS with the same best capability class, I read the description and chose the one that sounds most likely to be cropped (least number of limitations, lowest slope and/or highest PAWC).

Kaitlyn's more recent BASP layer has dominant entity codes already linked to polygons, so I don't need to repeat that.

``` {r LS - best LU - SPC links}

# list of target land system projects
BASP_LS_projects <- aggregate(AREA_HA ~ PROJECT_CODE + DOMINANT_ENTITY_TYPE, FUN = sum, data = BASP_grains, na.rm = TRUE) %>%
                    mutate(., Area_ha = round(AREA_HA)) %>%
                    filter(., DOMINANT_ENTITY_TYPE %in% c("LS", "LRA")) %>%
                    arrange(., desc(Area_ha))

# generate a list of target land systems to identify best LUs for
BASP_LS_ent_list <- aggregate(Area_ha ~ Clust_reg + DOMINANT_ENTITY_TYPE + PROJECT_CODE + DOMINANT_ENTITY_CODE, FUN = sum, data = BASP_grains, na.rm = TRUE) %>%
                    mutate(., Area_ha = round(Area_ha)) %>%
                    filter(., DOMINANT_ENTITY_TYPE %in% c("LS", "LRA")) %>%
                    arrange(., Clust_reg, PROJECT_CODE)

#############
# join comp number and name to entities 
LSY_COMP_PROJECTS <- dbGetQuery(SALI, "select 
                                              p.project_code, 
                                              p.label,
                                              p.land_comp_no as entity_no,
                                              c.name as LS_name
                                              from
                                              lsy_comp_projects p
                                              left join
                                              lsy_land_components c
                                              on
                                              p.land_comp_no = c.land_comp_no
                                              ")

BASP_LS_ent_list <- left_join(BASP_LS_ent_list, LSY_COMP_PROJECTS, by = c("PROJECT_CODE", "DOMINANT_ENTITY_CODE" = "LABEL"))

write.csv(BASP_LS_ent_list, "./1_spatial_framework/BASP_LS_ent_list.csv", row.names=F)

```

Using BASP_LS_ent_list I now manually check land system reports for the best land unit and its dominant soil, which I append to the file in a new column "SPC_NAME". The SPC name must precisely match the SPC NAME field in SPC_ATTRIBUTES in SALI. 

```{r}

# read in manual best LU attribution
LS_best_LU <- read.csv("./1_spatial_framework/BASP_LS_ent_list_LUs.csv", header=T) %>% select(., 1:9)

# bring in SPCs attributed to LUs
LSY_ATTRIBUTES1 <- dbGetQuery(SALI, "select
                                    a.land_comp_no,
                                    c.name,
                                    a.tax_unit_type,
                                    a.tax_unit_code
                                    from
                                    lsy_attributes a
                                    left join
                                    lsy_land_components c
                                    on
                                    a.land_comp_no = c.land_comp_no
                                    where
                                    a.att_no = 1")

# link SPCs to best LUs
LS_best_LU_SPC <- left_join(LS_best_LU, LSY_ATTRIBUTES1, by = c("Best_LU_name" = "NAME"))

# join in name and SPC number
SPC_NAME_NO_LUT <- dbGetQuery(SALI, "select 
                                       p.project_code,
                                       p.label,
                                       p.spc_no,
                                       a.name
                                       from
                                       spc_attrib_projects p
                                       left join
                                       spc_attributes a
                                       on
                                       p.spc_no = a.spc_no
                                       ")

LS_best_LU_SPC <- left_join(LS_best_LU_SPC, SPC_NAME_NO_LUT, by = c("PROJECT_CODE", "TAX_UNIT_CODE" = "LABEL"))

# summarise SPC lists for each cluster region
SPC_names2_FIB_IB <- group_by(LS_best_LU_SPC, Clust_reg, NAME) %>% 
                     summarise(., Area_ha = sum(Area_ha)) %>%
                     ungroup() %>%
                     arrange(., Clust_reg, desc(Area_ha))

# also generate list of SPCs with their names and SPC numbers, so I can link up site data in next step via tax unit codes
SPC_names2_ent_nos_FIB_IB <- group_by(LS_best_LU_SPC, Clust_reg, NAME, SPC_NO) %>%
                             summarise(., Area_ha = sum(Area_ha)) %>%
                             ungroup() %>%
                             arrange(., Clust_reg, NAME, desc(Area_ha))

```
Now I have SPC lists from both the SPC-based surveys and land systems surveys. I will combine them and summarise the total areas across each cluster region. Then they are ready for clustering.

```{r}

# generate SPC lists for clustering
SPC_names_FIB_IB <- rbind(SPC_names1_FIB_IB, SPC_names2_FIB_IB) %>%
                    group_by(Clust_reg, NAME) %>%
                    summarise(Area_ha = sum(Area_ha)) %>%
                    ungroup() %>%
                    arrange(NAME)

SPC_names_ent_nos_FIB_IB <- rbind(SPC_names1_ent_nos_FIB_IB, SPC_names2_ent_nos_FIB_IB) %>%
                            group_by(Clust_reg, NAME, SPC_NO) %>%
                            summarise(Area_ha = sum(Area_ha)) %>%
                            ungroup() %>%
                            arrange(Clust_reg, NAME, SPC_NO)

write.csv(SPC_names_FIB_IB, "./1_spatial_framework/SPC_names_FIB_IB.csv", row.names=F)
write.csv(SPC_names_ent_nos_FIB_IB, "./1_spatial_framework/SPC_names_ent_nos_FIB_IB.csv", row.names=F)

# count how many SPCs are recorded in each region
SPC_names_FIB_IB_regioncount <- group_by(SPC_names_FIB_IB, Clust_reg) %>%
  summarise(., count=n())

write.csv(SPC_names_FIB_IB_regioncount, "./1_spatial_framework/SPC_names_FIB_IB_regioncount.csv", row.names=F)

```

