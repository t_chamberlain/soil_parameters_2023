---
title: "Texture to PSD relationships"
author: "Tessa Chamberlain"
date: "14/05/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(DBI)
#library(ROracle)
#library(GSIF)
library(ithir)
library(modeest) #mode function
library(ggpmisc)
library(soiltexture)
library(RColorBrewer)

rm(list=ls(all=TRUE))
options(scipen = 999)

Sys.setenv(
  'TNS_ADMIN'   = "C:/Oracle",
  'ORACLE_HOME' = 'C:/Oracle/instantclient_12_1'
  )

```

``` {r Oracle connections, echo=FALSE, eval=TRUE}

drv    <- dbDriver('Oracle')

# SALI Connection
SALI <- dbConnect(drv,
                  username = 'chamberlaint',
                  password = 'Carrots1!',
                  dbname = 'saliprod',
                  host = 'lasun412',
                  port = 1521)

```

The script attempts to derive a look up table between field texture categories and particle size distributions. On reading the Minasny et al (2007) paper on this topic, I was going to use their Table 1 summaries of average clay, silt and sand values for texture groups. It was based on 17,979 samples available Australia-wide in the ASRIS system at that time. 

I figured I could reproduce those figures using data available in SALI, to have a Queensland-specific look up table.

```{r texture data}

textures <- dbGetQuery(SALI, "select 
                              project_code,
                              site_id,
                              obs_no,
                              horizon_no,
                              texture_code,
                              texture_qualifier
                              from
                              sit_horizons
                              ")

# remove NA textures
textures <- filter(textures, !is.na(TEXTURE_CODE))

# replace NA in qualifier with a zero length string
textures <- mutate(textures, TEXTURE_QUALIFIER = ifelse(is.na(TEXTURE_QUALIFIER), "", TEXTURE_QUALIFIER))

# there are a few invalid texture codes that I will either delete from the analysis, or recode
textures <- filter(textures, !(TEXTURE_CODE %in% c("GR", "FSCLZ", "CSC"))) %>%
            mutate(., TEXTURE_CODE = case_when(TEXTURE_CODE == "MS" ~ "S",
                                               TEXTURE_CODE == "LMS" ~ "LS",
                                               TEXTURE_CODE == "LFSY" ~ "FSL",
                                               TEXTURE_CODE == "LSY" ~ "SL",
                                               TEXTURE_CODE == "FSC" ~ "FSLC", 
                                               TEXTURE_CODE == "KSC" ~ "KSLC",
                                               TEXTURE_CODE == "SC" ~ "SLC",
                                               TEXTURE_CODE == "ZC" ~ "ZLC", 
                                               TRUE ~ TEXTURE_CODE))

write.csv(textures, "textures.csv")

# read back in
textures <- read.csv("textures.csv", header=T) %>% select(., -X)

```

```{r PSA data}

PSA_data <- dbGetQuery(SALI, "select
                              project_code,
                              site_id,
                              obs_no,
                              horizon_no,
                              sample_no,
                              lab_meth_code,
                              numeric_value
                              from
                              sit_lab_results
                              where
                              lab_meth_code in ('2Z2_Clay', '2Z2_Silt', '2Z2_FS', '2Z2_CS')
                              ")

PSA_data <- read.csv("PSA_data.csv", header=T)

PSA_data <- pivot_wider(PSA_data, names_from = "LAB_METH_CODE", values_from = "NUMERIC_VALUE") %>%
            arrange(., PROJECT_CODE, SITE_ID, OBS_NO, HORIZON_NO, SAMPLE_NO)

# remove any records with NAs
PSA_data <- na.omit(PSA_data)

# average results across horizons, where multiple samples may have been taken
## There are a few horizons with lots of samples (max is 12 for CQC 119), but these seem legit
## They are thick horizons that have just been sampled at every 10cm increment.
PSA_data_av <- group_by(PSA_data, PROJECT_CODE, SITE_ID, OBS_NO, HORIZON_NO) %>% 
               summarise(., Clay = round(mean(`2Z2_Clay`)), 
                            Silt = round(mean(`2Z2_Silt`)), 
                            FS = round(mean(`2Z2_FS`)), 
                            CS = round(mean(`2Z2_CS`)), 
                            Count = n())

# add PSA total to check data, and add total sand column
PSA_data_av <- mutate(PSA_data_av, Sand = FS + CS, Total = Clay + Silt + FS + CS)

# remove results <85% or >115%
PSA_data_av <- filter(PSA_data_av, between(Total, 85, 115))

# perform rescaling on remaining data
PSA_data_av <- mutate(PSA_data_av, Clay_resc = round(Clay/((Total)/100),0),
                      Silt_resc = round(Silt/((Total)/100),0),
                      FS_resc = round(FS/((Total)/100),0),
                      CS_resc = round(CS/((Total)/100),0),
                      Sand_resc = round(Sand/((Total)/100),0))

# just keep rescaled data
PSA_data_av_res <- select(PSA_data_av, -c(Clay, Silt, FS, CS, Sand, Total, Count)) %>%
                   mutate(., Total_resc = Clay_resc + Silt_resc + FS_resc + CS_resc)

write.csv(PSA_data_av_res, "PSA_data_av_res.csv")

# read back in
PSA_data_av_res <- read.csv("PSA_data_av_res.csv", header=T) %>% select(., -X)

```

Here I join texture and PSA data tables. I am also bringing in a texture code grouping that I created for the ASC rules work that I did with Bruce, to help arrange and interpret the data. It groups the texture codes up into broad categories (sands, loams, etc). 

```{r}

# join tables
All_data <- inner_join(textures, PSA_data_av_res, by = c("PROJECT_CODE", "SITE_ID", "OBS_NO", "HORIZON_NO"))

# get groupings
texture_groupings <- read.csv("Texture_Clay_LUT_BF_comments_counts_AIfilled.csv", header=T, stringsAsFactors = FALSE) %>%
  select(., Texture_group, Texture_group_name, TEXTURE_CODE, TEXTURE_QUALIFIER) %>%
  mutate(., TEXTURE_QUALIFIER = ifelse(is.na(TEXTURE_QUALIFIER), "", TEXTURE_QUALIFIER))

texture_groupings$Texture_group_name <- factor(texture_groupings$Texture_group_name, ordered = TRUE, c("Peats", "Sands", "Loamy sands", "Clayey sands", "Sandy loams", "Loams", "Silty loams", "Sandy clay loams", "Clay loams", "Silty clay loams", "Sandy and silty clays", "Light clays", "Light medium clays", "Medium clays", "Medium heavy clays", "Heavy clays"))

All_data <- left_join(All_data, texture_groupings, by = c("TEXTURE_CODE", "TEXTURE_QUALIFIER"))

# I don't generally want to include qualifiers, except for SCL-
All_data <- mutate(All_data, TEXTURE_CODE = case_when(TEXTURE_CODE == "SCL" & TEXTURE_QUALIFIER == "-" ~ paste0(TEXTURE_CODE, TEXTURE_QUALIFIER),
                                                      TRUE ~ TEXTURE_CODE)) %>%
            select(., -TEXTURE_QUALIFIER)

texture_code_list <- group_by(All_data, TEXTURE_CODE) %>% summarise()

All_data$TEXTURE_CODEf <- factor(All_data$TEXTURE_CODE, ordered = TRUE, c("AP", "CP", "GP", "HP", "IP", "LP", "SP", "FS", "KS", "KSS", "S", "LFS", "LKS", "LS", "CFS", "CKS", "CS", "FSL", "FSLZ", "KSL", "SL", "SLZ", "L", "LFSYZ", "ZL", "FSCL", "KSCL", "MSCL", "SCL-", "SCL", "SCLFS", "CL", "CLFS", "CLFSZ", "CLKS", "CLMS", "CLS", "CLZ", "ZCL", "FSLC", "FSLCZ", "KSLC", "LC", "LCFS", "LCKS", "LCS", "LCZ", "SLC", "ZLC", "ZLCFS", "FSLMC", "KSLMC", "LMC", "LMCFS", "LMCKS", "LMCS", "LMCZ", "SLMC", "ZLMC", "ZLMCS", "FSMC", "KSMC", "MC", "MCFS", "MCS", "MCZ", "SMC", "ZMC", "FSMHC", "KSMHC", "MHC", "MHCFS", "MHCS", "SMHC", "ZMHC", "FSHC", "HC", "KSHC", "SHC", "ZHC"))
     
# back up
write.csv(All_data, "All_data.csv")

# read back in
All_data <- read.csv("All_data.csv", header=T)  

```

Initially there are `r nrow(All_data)` records to work with that have both texture and particle size data.

The counts of data for each texture code are shown below.

``` {r, echo=FALSE}

colourCount = length(unique(All_data$Texture_group_name))
getPalette = colorRampPalette(brewer.pal(9, "Set1"))

# plot counts for each texture code
PSA_texture_count <- group_by(All_data, TEXTURE_CODEf, Texture_group_name) %>%
                     summarise(., Count = n())

ggplot(PSA_texture_count, aes(x=TEXTURE_CODEf, y=Count, fill=Texture_group_name)) +
  geom_bar(stat='identity', position='dodge') +
  scale_fill_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Data points",
       title = "Data count for each texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Data with spurious PSA totals removed."))

```

Below are boxplots showing the distribution of data across all texture codes, for each particle size fraction. These show that while the broad trends across texture groups are sensible, there are a large number of outlying and likely erroneous values in the data.

``` {r} 

#############   
### box plots by texture code

ggplot(All_data, aes(x=TEXTURE_CODEf, y=Clay_resc, group=TEXTURE_CODE, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Clay %",
       title = "Variation in clay content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Data with spurious PSA totals removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90))

ggplot(All_data, aes(x=TEXTURE_CODEf, y=Silt_resc, group=TEXTURE_CODE, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Silt %",
       title = "Variation in silt content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Data with spurious PSA totals removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70))

ggplot(All_data, aes(x=TEXTURE_CODEf, y=FS_resc, group=TEXTURE_CODE, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Fine sand %",
       title = "Variation in fine sand content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Data with spurious PSA totals removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))

ggplot(All_data, aes(x=TEXTURE_CODEf, y=CS_resc, group=TEXTURE_CODE, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Coarse sand %",
       title = "Variation in coarse sand content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Data with spurious PSA totals removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))

ggplot(All_data, aes(x=TEXTURE_CODEf, y=Sand_resc, group=TEXTURE_CODE, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Total sand %",
       title = "Variation in total sand content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Data with spurious PSA totals removed, remaining data rescaled where necessary. Box width signifies data counts"))

```

To remove problematic data, I am using a standard outlier detection method to identify outliers in the data (values outside +/- 1.5 * Inter Quartile Range). These are all removed.

After this, it is clear that there are still many data points that aren't sensible (e.g. clay textures with <10% clay). **I might apply some simple rules to remove these points that are clearly outside the sensible range - discuss this further with Brendan and AB**.

```{r}

#######################
# calculate boundaries for outlier detection (+/- 1/5*IQR)
outlier_detection <- group_by(All_data, TEXTURE_CODE) %>% 
                     summarize(Count = n(),
                               Clay_min = min(Clay_resc),
                               Clay_mean = round(mean(Clay_resc)),
                               Clay_max = max(Clay_resc),
                               Clay_q1 = quantile(Clay_resc, 0.25),
                               Clay_q3 = quantile(Clay_resc, 0.75),
                               Clay_bot = Clay_q1 - 1.5*IQR(Clay_resc),
                               Clay_top = Clay_q3 + 1.5*IQR(Clay_resc),
                               Silt_min = min(Silt_resc),
                               Silt_mean = round(mean(Silt_resc)),
                               Silt_max = max(Silt_resc),
                               Silt_q1 = quantile(Silt_resc, 0.25),
                               Silt_q3 = quantile(Silt_resc, 0.75),
                               Silt_bot = Silt_q1 - 1.5*IQR(Silt_resc),
                               Silt_top = Silt_q3 + 1.5*IQR(Silt_resc),
                               FS_min = min(FS_resc),
                               FS_mean = round(mean(FS_resc)),
                               FS_max = max(FS_resc),
                               FS_q1 = quantile(FS_resc, 0.25),
                               FS_q3 = quantile(FS_resc, 0.75),
                               FS_bot = FS_q1 - 1.5*IQR(FS_resc),
                               FS_top = FS_q3 + 1.5*IQR(FS_resc),
                               CS_min = min(CS_resc),
                               CS_mean = round(mean(CS_resc)),
                               CS_max = max(CS_resc),
                               CS_q1 = quantile(CS_resc, 0.25),
                               CS_q3 = quantile(CS_resc, 0.75),
                               CS_bot = CS_q1 - 1.5*IQR(CS_resc),
                               CS_top = CS_q3 + 1.5*IQR(CS_resc)) %>%
                      ungroup()

# link these to the data and filter out records outside these bounds
All_data2 <- left_join(All_data, select(outlier_detection, TEXTURE_CODE, Clay_bot, Clay_top, Silt_bot, Silt_top, FS_bot, FS_top, CS_bot, CS_top), by = "TEXTURE_CODE") %>%
            mutate(., Outlier = ifelse(Clay_resc < Clay_bot | Clay_resc > Clay_top | Silt_resc < Silt_bot | Silt_resc > Silt_top | FS_resc < FS_bot | FS_resc > FS_top | CS_resc < CS_bot | CS_resc > CS_top, "yes", "no"))

All_data_NoOutliers <- filter(All_data2, Outlier == "no")

#######################
# remove remaining clearly erroneous data
#All_data_NoOutliers <- filter(All_data_NoOutliers, !(Texture_group_name %in% c("Light clays", "Light medium clays", "Medium clays", "Medium heavy clays", "Heavy clays") & Clay_resc < 10))

# back up
write.csv(All_data_NoOutliers, "All_data_NoOutliers.csv")

```

Plot data ranges again now that outliers are removed.

``` {r }

#############   
### box plots by texture code

jpeg("Clay_variance_by_texture_code_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=TEXTURE_CODEf, y=Clay_resc, group=TEXTURE_CODEf, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Clay %",
       title = "Variation in clay content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. All errors and outliers removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90))
dev.off()

jpeg("Silt_variance_by_texture_code_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=TEXTURE_CODEf, y=Silt_resc, group=TEXTURE_CODEf, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Silt %",
       title = "Variation in silt content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. All errors and outliers removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90))
dev.off()

jpeg("FS_variance_by_texture_code_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=TEXTURE_CODEf, y=FS_resc, group=TEXTURE_CODEf, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Fine sand %",
       title = "Variation in fine sand content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. All errors and outliers removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

jpeg("CS_variance_by_texture_code_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=TEXTURE_CODEf, y=CS_resc, group=TEXTURE_CODEf, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Coarse sand %",
       title = "Variation in coarse sand content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. All errors and outliers removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

jpeg("Sand_variance_by_texture_code_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=TEXTURE_CODEf, y=Sand_resc, group=TEXTURE_CODEf, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(x = "Texture code",
       y = "Total sand %",
       title = "Variation in total sand content by texture code",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. All errors and outliers removed, remaining data rescaled where necessary. Box width signifies data counts")) + 
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

```

Now with main outliers removed, I can calculate stats by texture code.

```{r}

#############      
# calculate PSA stats by texture code
PSA_texture_stats <- group_by(All_data_NoOutliers, TEXTURE_CODEf, Texture_group_name) %>%
                     summarise(., Count = n(),
                                  Clay_min = min(Clay_resc),
                                  Clay_max = max(Clay_resc),
                                  Clay_mean = round(mean(Clay_resc)),
                                  Clay_med = round(median(Clay_resc)),
                                  Clay_sd = round(sd(Clay_resc)),
                                  Silt_min = min(Silt_resc),
                                  Silt_max = max(Silt_resc),
                                  Silt_mean = round(mean(Silt_resc)),
                                  Silt_med = round(median(Silt_resc)),
                                  Silt_sd = round(sd(Silt_resc)),
                                  FS_min = min(FS_resc),
                                  FS_max = max(FS_resc),
                                  FS_mean = round(mean(FS_resc)),
                                  FS_med = round(median(FS_resc)),
                                  FS_sd = round(sd(FS_resc)),
                                  CS_min = min(CS_resc),
                                  CS_max = max(CS_resc),
                                  CS_mean = round(mean(CS_resc)),
                                  CS_med = round(median(CS_resc)),
                                  CS_sd = round(sd(CS_resc)),
                                  Sand_min = min(Sand_resc),
                                  Sand_max = max(Sand_resc),
                                  Sand_mean = round(mean(Sand_resc)),
                                  Sand_med = round(median(Sand_resc)),
                                  Sand_sd = round(sd(Sand_resc)),
                                  Total_means = Clay_mean + Silt_mean + FS_mean + CS_mean)
                               
write.csv(PSA_texture_stats, "PSA_texture_stats.csv")

PSA_texture_LUT <- select(PSA_texture_stats, TEXTURE_CODEf, Texture_group_name, Count, Clay_mean, Silt_mean, FS_mean, CS_mean, Sand_mean, Total_means)

write.csv(PSA_texture_LUT, "PSA_texture_LUT.csv")

```



Summarise data more broadly by texture group.

``` {r}

#############   
# calculate PSA stats by texture group
PSA_texture_group_stats <- group_by(All_data_NoOutliers, Texture_group_name) %>%
                     summarise(., Count = n(),
                                  Clay_min = min(Clay_resc),
                                  Clay_max = max(Clay_resc),
                                  Clay_mean = round(mean(Clay_resc)),
                                  Clay_med = round(median(Clay_resc)),
                                  Clay_sd = round(sd(Clay_resc)),
                                  Silt_min = min(Silt_resc),
                                  Silt_max = max(Silt_resc),
                                  Silt_mean = round(mean(Silt_resc)),
                                  Silt_med = round(median(Silt_resc)),
                                  Silt_sd = round(sd(Silt_resc)),
                                  FS_min = min(FS_resc),
                                  FS_max = max(FS_resc),
                                  FS_mean = round(mean(FS_resc)),
                                  FS_med = round(median(FS_resc)),
                                  FS_sd = round(sd(FS_resc)),
                                  CS_min = min(CS_resc),
                                  CS_max = max(CS_resc),
                                  CS_mean = round(mean(CS_resc)),
                                  CS_med = round(median(CS_resc)),
                                  CS_sd = round(sd(CS_resc)),
                                  Sand_min = min(Sand_resc),
                                  Sand_max = max(Sand_resc),
                                  Sand_mean = round(mean(Sand_resc)),
                                  Sand_med = round(median(Sand_resc)),
                                  Sand_sd = round(sd(Sand_resc)),
                                  Total_cal = Clay_med + Silt_med + FS_med + CS_med)

write.csv(PSA_texture_group_stats, "PSA_texture_group_stats.csv")

#############   
### box plots by texture group
jpeg("Clay_variance_by_texture_group_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=Texture_group_name, y=Clay_resc, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(legend.position = "none") +
  labs(x = "Texture group",
       y = "Clay %",
       title = "Variation in clay content by texture group",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Spurious PSA data removed, remaining data rescaled where necessary. Box width signifies data counts")) +
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

jpeg("Silt_variance_by_texture_group_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=Texture_group_name, y=Silt_resc, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(legend.position = "none") +
  labs(x = "Texture group",
       y = "Silt %",
       title = "Variation in silt content by texture group",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Spurious PSA data removed, remaining data rescaled where necessary. Box width signifies data counts")) +
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

jpeg("FS_variance_by_texture_group_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=Texture_group_name, y=FS_resc, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(legend.position = "none") +
  labs(x = "Texture group",
       y = "Fine sand %",
       title = "Variation in fine sand content by texture group",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Spurious PSA data removed, remaining data rescaled where necessary. Box width signifies data counts")) +
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

jpeg("CS_variance_by_texture_group_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data_NoOutliers, aes(x=Texture_group_name, y=CS_resc, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(legend.position = "none") +
  labs(x = "Texture group",
       y = "Coarse sand %",
       title = "Variation in coarse sand content by texture group",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Spurious PSA data removed, remaining data rescaled where necessary. Box width signifies data counts")) +
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

jpeg("Sand_variance_by_texture_group_NoOutliers.jpg", width=2000, height=800)
ggplot(All_data, aes(x=Texture_group_name, y=Sand_resc, color=Texture_group_name)) +
  geom_boxplot(lwd=0.8, varwidth=T) +
  scale_color_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(colourCount)) +
  theme_minimal() +
  theme(legend.position = "none") +
  labs(x = "Texture group",
       y = "Total sand %",
       title = "Variation in total sand content by texture group",
       subtitle = paste("Based on ", nrow(All_data), "data points in SALI. Spurious PSA data removed, remaining data rescaled where necessary. Box width signifies data counts")) +
  scale_y_continuous(breaks = c(0,10,20,30,40,50,60,70,80,90,100))
dev.off()

```

Clay vs sand plots for mean values by texture code and texture group.

``` {r} 

### Texture code means

# Clay vs Sand
jpeg("Clay_vs_Sand_texture_codes.jpg", width=1000, height=850)
ggplot(PSA_texture_stats, aes(x=Clay_mean, y=Sand_mean, color=Silt_mean)) +
  geom_point(aes(size=Count)) +
  scale_color_gradientn(colours = rainbow(5)) +
  geom_text(data=PSA_texture_stats, aes(label=TEXTURE_CODEf), color="black", cex = 2.5, hjust=1.1, vjust=1.1) +
  theme_minimal(base_size = 15) +
  xlim(0,100) +
  ylim(0,100) +
  labs(x = "Clay %",
       y = "Total sand %",
       title = "Clay vs total sand content, mean values per texture code")
dev.off()

# Triangle plot
PSA_texture_stats <- mutate(PSA_texture_stats, CLAY = Clay_mean, SILT = Silt_mean, SAND = Sand_mean)

PSA_texture_stats_n <- TT.normalise.sum(tri.data = PSA_texture_stats)

jpeg("Triangle_plot_texture_codes.jpg", width = 700, height = 700)
TT.plot(
class.sys = "AU2.TT",
tlr.an = c(60,60,60),
tri.data = PSA_texture_stats_n,
tri.sum.tst = FALSE,
main = "Mean particle sizes per texture code"
)
dev.off()


### Texture group means

# Clay vs Sand
jpeg("Clay_vs_Sand_texture_groups.jpg", width=1000, height=850)
ggplot(PSA_texture_group_stats, aes(x=Clay_mean, y=Sand_mean, color=Silt_mean)) +
  geom_point(aes(size=Count)) +
  scale_color_gradientn(colours = rainbow(5)) +
  geom_text(data=PSA_texture_group_stats, aes(label=Texture_group_name), color="black", hjust=1.1, vjust=1.1) +
  theme_minimal(base_size = 15) +
  xlim(0,100) +
  ylim(0,100) +
  labs(x = "Clay %",
       y = "Total sand %",
       title = "Clay vs total sand content, mean values per texture group")
dev.off()

# Triangle plot
PSA_texture_group_stats <- mutate(PSA_texture_group_stats, CLAY = Clay_mean, SILT = Silt_mean, SAND = Sand_mean)

PSA_texture_group_stats_n <- TT.normalise.sum(tri.data = PSA_texture_group_stats)

jpeg("Triangle_plot_texture_groups.jpg", width = 700, height = 700)
TT.plot(
class.sys = "AU2.TT",
tlr.an = c(60,60,60),
tri.data = PSA_texture_group_stats_n,
main = "Mean particle sizes per texture group",
arrows.show = TRUE)
dev.off()

          
```

