---
title: "2g_estimating_15bar_ADMC"
author: "Tessa Chamberlain"
date: "20/05/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(ROracle)
library(ggpmisc)
library(kableExtra)

rm(list=ls(all=TRUE))
options(scipen = 999)

```

``` {r Oracle connections, echo=FALSE, eval=TRUE}

drv    <- dbDriver('Oracle')

# SALI Connection
SALI <- dbConnect(drv,
                  username = 'chamberlaint',
                  password = 'Monday16!',
                  dbname = 'saliprod',
                  host = 'lasun412',
                  port = 1521)

```

I want to use a pedotransfer function to estimate 15bar moisture content, and ADMC, where it isn't available for sites in my SPC attributes script. I was going to use Roger Shaw's equations, but have found they don't perform very well at higher clay contents. So I will have a go at deriving new equations quickly myself from the data currently available in SALI.

```{r}

### get data ------------------------------------------------------------------------------------

data <- dbGetQuery(SALI, "select
                            project_code||'-'||site_id||'-'||obs_no as ID,
                            horizon_no,
                            sample_no,
                            lab_meth_code, 
                            numeric_value
                            from
                            sit_lab_results
                            where
                            lab_meth_code in ('2Z2_Clay', '2Z2_Silt', '2Z2_FS', '2Z2_CS', '2A1', '2E1', '2E2')
                            ") %>%
         filter(., !is.na(NUMERIC_VALUE))

# first remove all negative values
data <- filter(data, NUMERIC_VALUE >= 0)

### remove bad PSA totals and rescale ----------------------------------------------------------

dataW <- pivot_wider(data, names_from = "LAB_METH_CODE", values_from = "NUMERIC_VALUE") %>%
            arrange(., ID, HORIZON_NO, SAMPLE_NO) %>%
            rename(., Clay = `2Z2_Clay`, 
                      Silt = `2Z2_Silt`, 
                      FS = `2Z2_FS`, 
                      CS = `2Z2_CS`, 
                      ADMC = `2A1`, 
                      m15bar = `2E1`, 
                      m3rdbar = `2E2`) %>%
            mutate(., Total = Clay + Silt + FS + CS)

# remove results <85% or >115%
dataW <- filter(dataW, between(Total, 85, 115))

# perform rescaling on remaining data
dataW <- mutate(dataW, Clay = round(Clay/((Total)/100),0),
                      Silt = round(Silt/((Total)/100),0),
                      FS = round(FS/((Total)/100),0),
                      CS = round(CS/((Total)/100),0)) %>%
         select(., -c("Total", "Silt", "FS", "CS"))

# pivot longer
data <- pivot_longer(dataW, cols = m15bar:ADMC, names_to = "variable", values_to = "value") %>%
        filter(., !is.na(value))

```

# Check for statistical outliers

I've already cleaned the particle size data, so clay values are all within a sensible range. Check and remove outliers in ADMC and 15bar.

``` {r}

# boxplot
ggplot(data, aes(y=value)) +
  geom_boxplot(fill="lightgrey") +
  facet_grid(. ~ variable)

# determine IQR
out_rng <- group_by(data, variable) %>%
            summarise(., count = n(),
                         min = min(value),
                         mean = round(mean(value)),
                         max = max(value),
                         q1 = quantile(value, 0.25),
                         q3 = quantile(value, 0.75),
                         bot = q1 - 1.5*IQR(value),
                         top = q3 + 1.5*IQR(value))

# The IQR outlier analysis is a bit tougher than SALI QC ranges - so stick with SALI limits
data <- filter(data, !(variable == "ADMC" & value < 0.4)) %>%
        filter(., !(variable == "ADMC" & value > 35)) %>%
        filter(., !(variable == "m15bar" & value < 0.3)) %>%
        filter(., !(variable == "m15bar" & value > 50)) %>%
        filter(., !(variable == "m3rdbar" & value < 0.3)) %>%
        filter(., !(variable == "m3rdbar" & value > 77))

## TABLE 3
dataSumm <- group_by(data, variable) %>% summarise(., count=n(),
                                                   min = min(value),
                                                   mean = round(mean(value),2),
                                                   median = round(median(value),2),
                                                   max = round(max(value),2),
                                                   sd = round(sd(value),2))

dataSumm %>%
  kable(caption = "Table: summary of clay, ADMC and 15bar data, outliers removed",
        table.attr = "style='width:65%;'") %>%
  kable_styling()

# pivot wide again for analysis
dataW <- pivot_wider(data, names_from = variable, values_from = value)

```

```{r}

ggplot(dataW, aes(x=Clay, y=ADMC)) +
  geom_point() +
  geom_smooth(method=lm, formula=y~x, se=T, color="red") +
  stat_poly_eq(formula = y ~ x, aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")), parse = TRUE, label.x = 0.045, label.y = 0.9, color="red")

ggplot(dataW, aes(x=Clay, y=m15bar)) +
  geom_point() +
  geom_smooth(method=lm, formula=y~x, se=T, color="red") +
  stat_poly_eq(formula = y ~ x, aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")), parse = TRUE, label.x = 0.045, label.y = 0.9, color="red")

ggplot(dataW, aes(x=Clay, y=m3rdbar)) +
  geom_point() +
  geom_smooth(method=lm, formula=y~x, se=T, color="red") +
  stat_poly_eq(formula = y ~ x, aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")), parse = TRUE, label.x = 0.045, label.y = 0.9, color="red")

data2 <- pivot_longer(dataW, cols=c("m3rdbar", "m15bar", "ADMC"), names_to = "variable", values_to = "value")

# on one plot
ggplot(data2, aes(x=Clay, y=value, color=variable)) +
  geom_point(size=1) +
  scale_color_brewer(palette="Set1") +
  geom_smooth(method=lm) +
  stat_poly_eq(formula = y ~ x, aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")), parse = TRUE)

lm_admc <- lm(ADMC ~ Clay, data=dataW)
lm_15bar <- lm(m15bar ~ Clay, data=dataW)
lm_3rdbar <- lm(m3rdbar ~ Clay, data=dataW)
summary(lm_admc)
summary(lm_15bar)
summary(lm_3rdbar)

```


