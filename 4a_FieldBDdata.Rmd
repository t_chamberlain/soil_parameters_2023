---
title: "Tidying_soil_phys_data"
author: "Tessa Chamberlain"
date: "22/11/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(ithir)

rm(list=ls(all=TRUE))
options(stringsAsFactors = FALSE)
root_dir <- dirname(getwd())
currentDate <- Sys.Date() 

```

I need to tidy up the measured bulk density data for two purposes:

- to feed into APSIM parameter generation for Reef parameter sets and for John Eden
- to define a PAWCER BD equation correction factor, to get predicted BD values closer to reality for estimating ERD.

# Import Jenny's Mackay data

```{r}

# Import Jenny's data where there are multiple reps for a site
bd_Jenny1_MW <- read.csv("./_field_data/Mackay_Root_Extract_2020.csv", header=T, strip.white=T) %>%
                select(., NAME, PROJECT_CODE, SITE_ID, Rep, Depth..cm., VALUE = Bulk.Density) %>%
                separate(., Depth..cm., c("UD", "LD"), "-") %>%
                mutate(., UD = as.numeric(UD), 
                          LD = as.numeric(LD))

bd_Jenny2_MW <- read.csv("./_field_data/MISCQ_42_47_Moisture_BD.csv", header=T, strip.white=T) %>%
                select(., NAME, PROJECT_CODE, SITE_ID, Rep, Depth.cm, VALUE = Bulk.Density) %>%
                separate(., Depth.cm, c("UD", "LD"), "-") %>%
                mutate(., UD = as.numeric(UD), 
                          LD = as.numeric(LD), 
                          Rep = as.character(Rep))

bd_Jenny_Calen <- read.csv("./_field_data/Calen-APSIM_parameters_TC.csv", header=T, skip=1) %>%
                  select(., PROJECT_CODE:Depth, VALUE = Bulk.Density) %>%
                  separate(., col=Depth, into=c("UD", "LD"), sep="-") %>%
                  mutate(., UD = as.numeric(UD),
                            LD = as.numeric(LD)) %>%
                  filter(., !is.na(VALUE))

bd_Jenny_Sandi <- read.csv("./_field_data/Sandiford-APSIM_parameters_TC.csv", header=T, skip=1) %>%
                  select(., PROJECT_CODE:Depth, VALUE = Bulk.Density) %>%
                  separate(., col=Depth, into=c("UD", "LD"), sep="-") %>%
                  mutate(., UD = as.numeric(UD),
                            LD = as.numeric(LD)) %>%
                  filter(., !is.na(UD))

bd_Jenny_VicP <- read.csv("./_field_data/VictoriaPlains-APSIM_parameters_TC.csv", header=T, skip=1) %>%
                  select(., PROJECT_CODE:Depth, VALUE = Density) %>%
                  separate(., col=Depth, into=c("UD", "LD"), sep="-") %>%
                  mutate(., UD = as.numeric(UD),
                            LD = as.numeric(LD)) %>%
                  filter(., !is.na(UD))

bd_JennyAll_MW <- bind_rows(bd_Jenny1_MW, bd_Jenny2_MW, bd_Jenny_Calen, bd_Jenny_Sandi, bd_Jenny_VicP) %>%
         mutate(., ID = paste0(SITE_ID, "-", Rep),
                   NAME_ID = paste0(NAME, "_", PROJECT_CODE, "-", SITE_ID, "-1"),
                   midPnt_cm = (UD+LD)/2) %>%
         select(., ID, UD, LD, VALUE, NAME, NAME_ID, PROJECT_CODE, SITE_ID, Rep, midPnt_cm) %>%
         filter(., ID != "87-3") %>% #there is one record for a third rep - can't spline this
         filter(., ID != "309-M1") %>% #also one with no BD value
         arrange(., ID, UD)

Site_NAME_LUT <- group_by(bd_JennyAll_MW, ID, PROJECT_CODE, SITE_ID, NAME) %>%
                 summarise() %>%
                 ungroup()

## put aside uppermost raw values to replace the incorrect 0-10 values created by the spline
bd_JennyAll_MW_surf <- group_by(bd_JennyAll_MW, NAME_ID, Rep) %>%
              slice(which.min(UD)) %>%
              ungroup() %>%
              select(., ID, VALUE)

```

# Spline Jenny's data for APSIM

Now spline the BD data to get consistent depths. **Depth intervals set for APSIM parameterisation**

***NOTE - splines are all returning a value of 1.0 at the top of the profiles. Not sure why. The raw data has values for 0-10 so the splined 0-10 should be the same. For the moment I will manually replace the incorrect splined values with the raw values.

```{r}

### Spline data for multiple reps
Jenny_out <- ithir::ea_spline(bd_JennyAll_MW, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,50,70,90,120,150,170)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- Jenny_out[["harmonised"]]

Jenny_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`150-170 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2))

# join descriptors back on
Jenny_BD <- mutate(Jenny_BD, id2 = id, midPnt_cm = (UD + LD)/2) %>%
             separate(., id2, c("SITE_ID", "Rep"), "-") %>%
             mutate(., SITE_ID = as.numeric(SITE_ID))

Jenny_BD <- left_join(Jenny_BD, Site_NAME_LUT, by = c("id" = "ID", "SITE_ID")) %>%
             mutate(., NAME_ID = paste0(NAME, "_", PROJECT_CODE, "-", SITE_ID, "-1"))

# join surf raw data to replace incorrect top of Jennys
Jenny_BD <- left_join(Jenny_BD, bd_JennyAll_MW_surf, by = c("id" = "ID")) %>%
             mutate(., BD = if_else(UD == 0, VALUE, BD)) %>%
             select(., -VALUE)

write.csv(Jenny_BD, "./_field_data/Jenny_BD.csv", row.names=F)
Jenny_BD <- read.csv("./_field_data/Jenny_BD.csv", header=T)

# calculate average value per site
Jenny_BD_sit <- group_by(Jenny_BD, NAME, PROJECT_CODE, SITE_ID, UD, LD) %>%
                 summarise(., BD = round(mean(BD, na.rm=T),2)) %>%
                 ungroup() %>%
                 mutate(., midPnt_cm = (UD + LD)/2,
                           ID = paste0(PROJECT_CODE, "-", SITE_ID, "-1"),
                           NAME_ID = paste0(NAME, "_", ID),
                           Source = "Jenny splined")

write.csv(Jenny_BD_sit, "./_field_data/Jenny_BD_sit.csv", row.names=F)
#Jenny_BD_sit <- read.csv("./_field_data/Jenny_BD_sit.csv", header=T)


# plot data - reps and site average
jpeg("./_field_data/BD_JennyAll_bySite_MW.jpg", height=1500, width=3000)
ggplot(bd_JennyAll_MW, aes(x=VALUE, y=midPnt_cm)) +
  geom_point(col="grey", size=5) +
  geom_point(data=Jenny_BD, aes(x=BD, y=midPnt_cm), col="orange", size=5) +
  geom_point(data=Jenny_BD_sit, aes(x=BD, y=midPnt_cm), col="red", size=6) +
  theme_bw() +
  theme(text = element_text(size=25), panel.grid = element_line(colour = 'black')) +
  labs(title = "Splined field BD data",
       subtitle = "Grey = raw rep data / Orange = splined rep data / Red = site average splined data") +
  scale_y_continuous(limits=c(170,0), trans = "reverse") +
  facet_wrap(~NAME_ID, ncol=6)
dev.off()

```

# Spline Jenny's data for HOWLEAKY

Now spline the BD data to get consistent depths. **Depth intervals set for HOWLEAKY parameterisation**

***NOTE - splines are all returning a value of 1.0 at the top of the profiles. Not sure why. The raw data has values for 0-10 so the splined 0-10 should be the same. For the moment I will manually replace the incorrect splined values with the raw values.

```{r}

### Spline data for multiple reps
Jenny_out <- ithir::ea_spline(bd_JennyAll_MW, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,60,90,130,150,170)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- Jenny_out[["harmonised"]]

Jenny_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`150-170 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2))

# join descriptors back on
Jenny_BD <- mutate(Jenny_BD, id2 = id, midPnt_cm = (UD + LD)/2) %>%
             separate(., id2, c("SITE_ID", "Rep"), "-") %>%
             mutate(., SITE_ID = as.numeric(SITE_ID))

Jenny_BD <- left_join(Jenny_BD, Site_NAME_LUT, by = c("id" = "ID", "SITE_ID")) %>%
             mutate(., NAME_ID = paste0(NAME, "_", PROJECT_CODE, "-", SITE_ID, "-1"))

# join surf raw data to replace incorrect top of Jennys
Jenny_BD <- left_join(Jenny_BD, bd_JennyAll_MW_surf, by = c("id" = "ID")) %>%
             mutate(., BD = if_else(UD == 0, VALUE, BD)) %>%
             select(., -VALUE)

write.csv(Jenny_BD, "./_field_data/Jenny_BD_HL.csv", row.names=F)
Jenny_BD <- read.csv("./_field_data/Jenny_BD_HL.csv", header=T)

# calculate average value per site
Jenny_BD_sit <- group_by(Jenny_BD, NAME, PROJECT_CODE, SITE_ID, UD, LD) %>%
                 summarise(., BD = round(mean(BD, na.rm=T),2)) %>%
                 ungroup() %>%
                 mutate(., midPnt_cm = (UD + LD)/2,
                           ID = paste0(PROJECT_CODE, "-", SITE_ID, "-1"),
                           NAME_ID = paste0(NAME, "_", ID),
                           Source = "Jenny splined")

write.csv(Jenny_BD_sit, "./_field_data/Jenny_BD_sit_HL.csv", row.names=F)
#Jenny_BD_sit <- read.csv("./_field_data/Jenny_BD_sit_HL.csv", header=T)


# plot data - reps and site average
jpeg("./_field_data/BD_JennyAll_bySite_MW_HL.jpg", height=1500, width=3000)
ggplot(bd_JennyAll_MW, aes(x=VALUE, y=midPnt_cm)) +
  geom_point(col="grey", size=5) +
  geom_point(data=Jenny_BD, aes(x=BD, y=midPnt_cm), col="orange", size=5) +
  geom_point(data=Jenny_BD_sit, aes(x=BD, y=midPnt_cm), col="red", size=6) +
  theme_bw() +
  theme(text = element_text(size=25), panel.grid = element_line(colour = 'black')) +
  labs(title = "Splined field BD data",
       subtitle = "Grey = raw rep data / Orange = splined rep data / Red = site average splined data") +
  scale_y_continuous(limits=c(170,0), trans = "reverse") +
  facet_wrap(~NAME_ID, ncol=6)
dev.off()

```

# Spline Jenny's data for correction factor

Repeat of above splining, but set to 10cm increments to use in BD correction factor equation

```{r}

### Spline data for multiple reps
Jenny_out <- ithir::ea_spline(bd_JennyAll_MW, var.name = "VALUE", lam = 0.1, d = t(c(0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- Jenny_out[["harmonised"]]

Jenny_BD10 <- pivot_longer(harmondat, cols = `0-10 cm`:`160-170 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2))

# join descriptors back on
Jenny_BD10 <- mutate(Jenny_BD10, id2 = id, midPnt_cm = (UD + LD)/2) %>%
             separate(., id2, c("SITE_ID", "Rep"), "-") %>%
             mutate(., SITE_ID = as.numeric(SITE_ID))

Jenny_BD10 <- left_join(Jenny_BD10, Site_NAME_LUT, by = c("id" = "ID", "SITE_ID")) %>%
             mutate(., NAME_ID = paste0(NAME, "_", PROJECT_CODE, "-", SITE_ID, "-1"))

# join surf raw data to replace incorrect top of splines
Jenny_BD10 <- left_join(Jenny_BD10, bd_JennyAll_MW_surf, by = c("id" = "ID")) %>%
             mutate(., BD = if_else(UD == 0, VALUE, BD)) %>%
             select(., -VALUE)

write.csv(Jenny_BD10, "./_field_data/Jenny_BD10.csv", row.names=F)
#Jenny_BD10 <- read.csv("./_field_data/Jenny_BD10.csv", header=T)

# calculate average value per site
Jenny_BD10_sit <- group_by(Jenny_BD10, NAME, PROJECT_CODE, SITE_ID, UD, LD) %>%
                 summarise(., BD = round(mean(BD, na.rm=T),2)) %>%
                 ungroup() %>%
                 mutate(., midPnt_cm = (UD + LD)/2,
                           ID = paste0(PROJECT_CODE, "-", SITE_ID, "-1"),
                           NAME_ID = paste0(NAME, "_", ID),
                           Source = "Jenny splined")

write.csv(Jenny_BD10_sit, "./_field_data/Jenny_BD10_sit.csv", row.names=F)
Jenny_BD10_sit <- read.csv("./_field_data/Jenny_BD10_sit.csv", header=T)

```

#### Warrick's Mackay data

Initially, I compiled all the Mackay raw data myself above. Then Warrick sent me his copy of the data, where he has done some data cleansing and collated reps.

It has too many intervals so I'm quickly averaging some layers to get the layers I want.

Then feeding this back into the compliation of data in the first chunk above.

```{r}

BD_Warrick <- read.csv("./_field_data/Mackay_BD_avg_spline.csv", header=T)

#### convert to depths for APSIM parameters
BD_Warrick1 <- group_by(BD_Warrick, ID) %>%
         summarise(., UD = min(Upper_Depth),
                      LD = max(Lower_Depth),
                      across(S66:S88, mean)) %>%
         mutate(., across(S66:S88, round, 2)) %>%
         select(., -ID)

BD_Warrick1 <- pivot_longer(BD_Warrick1, cols = S66:S88, names_to = "ID", values_to = "value")
BD_Warrick1$ID <- gsub("S", "MISCQ-", BD_Warrick1$ID)
BD_Warrick1 <- mutate(BD_Warrick1, ID = paste0(ID, "-1"))

# add site info on
site_SPCs_MW <- read.csv("./_field_data/Sites_Mackay_Version 2_Jan 2021_Sept2021_Nov2021_4.csv", header=T, strip.white=T) %>%
                filter(., !is.na(Site.number)) %>%
                rename(., NAME = SPC.confirmed) %>%
                mutate(., ID = paste0("MISCQ-", Site.number, "-1"),
                          NAME = case_when(NAME == "Uruba alluvial-colluvial variant (Ub1)" ~ "Uruba Alluvial-Colluvial Variant",
                                             NAME == "S tHelens" ~ "St Helens",
                                             NAME == "Victoria Plans" ~ "Victoria Plains",
                                             TRUE ~ NAME)) %>%
                select(., ID, NAME)

BD_Warrick1 <- left_join(BD_Warrick1, site_SPCs_MW, by = "ID") %>%
              mutate(., Source = "Warrick")

write.csv(BD_Warrick1, "./_field_data/BD_Warrick1.csv", row.names=F)

#### convert to 10cm increments for BD correction factor
BD_Warrick2 <- group_by(BD_Warrick, ID_10cm) %>%
         summarise(., UD = min(Upper_Depth),
                      LD = max(Lower_Depth),
                      across(S66:S88, mean)) %>%
         mutate(., across(S66:S88, round, 2)) %>%
         select(., -ID_10cm)

BD_Warrick2 <- pivot_longer(BD_Warrick2, cols = S66:S88, names_to = "ID", values_to = "value")
BD_Warrick2$ID <- gsub("S", "MISCQ-", BD_Warrick2$ID)
BD_Warrick2 <- mutate(BD_Warrick2, ID = paste0(ID, "-1"),
                      Source = "Warrick")

write.csv(BD_Warrick2, "./_field_data/BD_Warrick2.csv", row.names=F)
BD_Warrick2 <- read.csv("./_field_data/BD_Warrick2.csv", header=T)

```

# Combine Jenny's and Warrick's data for APSIM parameters

``` {r}

# Join Warrick's splined data onto my splined Jenny data
BD_MW_all <- rbind(select(Jenny_BD_sit, ID, UD, LD, value = BD, NAME, Source), BD_Warrick1) %>%
             mutate(., midPnt_cm = (UD + LD)/2,
                       ID_NAME = paste0(NAME, "_", ID)) %>%
             arrange(., ID_NAME, UD)

write.csv(BD_MW_all, "./_field_data/BD_MW_all.csv", row.names=F)

# plot all site level data
jpeg("./_field_data/BD_MW_all.jpg", height=2000, width=4000)
ggplot(BD_MW_all, aes(x=value, y=midPnt_cm)) +
  geom_point(color = "blue", size=5) +
  theme_bw() +
  theme(text = element_text(size=25), panel.grid = element_line(colour = 'black')) +
  labs(title = "Mackay field BD data") +
  scale_y_continuous(limits=c(170,0), trans = "reverse") +
  facet_wrap(~ID_NAME, ncol=9)
dev.off()

# calculate average value per NAME (this is prob too rough - just pick one good site)
BD_MW_all_byNAME <- group_by(BD_MW_all, NAME, UD, LD) %>%
                    summarise(., BD = round(mean(value, na.rm=T),2),
                                 cnt_sites = n()) %>%
                    ungroup() %>%
                    mutate(., midPnt_cm = (UD + LD)/2)

write.csv(BD_MW_all_byNAME, "./_field_data/BD_MW_all_byNAME.csv", row.names=F)

# plot data - by reps and NAME average
jpeg("./_field_data/BD_FieldAll_byNAME_MW.jpg", height=2000, width=4000)
ggplot(bd_JennyAll_MW, aes(x=VALUE, y=midPnt_cm)) +
  geom_point(col="grey", size=5) +
  geom_point(data=BD_MW_all, aes(x=value, y=midPnt_cm), col="orange", size=5) +
  geom_point(data=BD_MW_all_byNAME, aes(x=BD, y=midPnt_cm), col="red", size=6) +
  theme_bw() +
  theme(text = element_text(size=25), panel.grid = element_line(colour = 'black')) +
  labs(title = "Splined field BD data",
       subtitle = "Grey = raw rep data / Orange = splined rep data / Red = SPC NAME average") +
  scale_y_continuous(limits=c(170,0), trans = "reverse") +
  facet_wrap(~NAME, ncol=9)
dev.off()

```

# Combine Jenny's and Warrick's data for correction factor (10cm)

``` {r}

# Join Warrick's splined data onto my splined Jenny data
BD_MW_all_10cm <- rbind(select(Jenny_BD10_sit, ID, UD, LD, value = BD, Source), BD_Warrick2) %>%
             arrange(., ID, UD)

write.csv(BD_MW_all_10cm, "./_field_data/BD_MW_all_10cm.csv", row.names=F)

```

#### July 2022 - collate BD data for other regions

I'm doing this to help define a PAWCER BD correction factor. I proved that a correction factor is useful with the Mackay data only, but I want to include data from a wider range of soils and locations to make the correction factor more universally applicable. There are some known BD datasets that aren't included here as they would take too long to get into a suitable format and connection to SALI sites. Pete Binns will be working on getting all data into SALI in coming months.

```{r}

# I've collated the CQSFS data where it relates to SFS sites
CQSFS <- read.csv("./_field_data/CQSFS_BD_data_for_SALI.csv", header=T) %>% rename(SAMPLE_NO = CLS_SAMPLE_NO, VALUE = BD)
CQSFS$HORIZON_NO <- NA
CQSFS$variable <- "BD"
CQSFS <- select(CQSFS, PROJECT_CODE, SITE_ID, OBS_NO, HORIZON_NO, SAMPLE_NO, UD, LD, variable, VALUE)  %>%
         mutate(., Source = "CQSFS")

# this is data that Pete is in the process of collating for data entry
End <- read.csv("./_field_data/Enderlin_data.csv", header=F) %>% mutate(., Source = "Enderlin")
Mull <- read.csv("./_field_data/Mullins&Vandersee.csv", header=F) %>% mutate(., Source = "Mullins")
Glen <- read.csv("./_field_data/Glenburn_data.csv", header=F) %>% mutate(., Source = "Glenburn")
MtMort <- read.csv("./_field_data/Mt_Mort_data.csv", header=F) %>% mutate(., Source = "MtMort")
Gard <- read.csv("./_field_data/Gardner_data.csv", header=F) %>% mutate(., Source = "Gardner")
Spring <- read.csv("./_field_data/Springvale_data.csv", header=F) %>% mutate(., Source = "Springvale")
Christ <- read.csv("./_field_data/Christodoulou_data.csv", header=F) %>% mutate(., Source = "Christodoulou")

Pete_all <- rbind(End, Mull, Glen, MtMort, Gard, Spring, Christ)
Pete_all <- Pete_all[,c(1:6,13,14,22)]
colnames(Pete_all) <- c("PROJECT_CODE", "SITE_ID", "OBS_NO", "HORIZON_NO", "SAMPLE_NO", "depth", "variable", "VALUE", "Source")
Pete_all <- Pete_all %>% separate(depth, c("UD", "LD"), sep = " - ") %>%
            mutate_at(vars(UD, LD), .funs=as.numeric)
Pete_BD <- filter(Pete_all, variable == "BD") %>%
           mutate(., UD = UD * 100,
                     LD = LD * 100)

July_BD <- rbind(CQSFS, Pete_BD) %>%
           mutate(., ID = paste(PROJECT_CODE, SITE_ID, OBS_NO, sep="-")) %>%
           select(., ID, UD, LD, VALUE, Source)
write.csv(July_BD, "./_field_data/July_BD.csv", row.names=F)

## put aside uppermost raw values to replace the incorrect 0-10 values created by the spline
July_BD_surf <- group_by(July_BD, ID) %>%
              slice(which.min(UD)) %>%
              ungroup() %>%
              select(., ID, VALUE)

##############################
### Spline data for correction factor
spline_out <- ea_spline(July_BD, var.name = "VALUE", lam = 0.1, d = t(c(0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- spline_out[["harmonised"]]

spline_July_BD_10cm <- pivot_longer(harmondat, cols = `0-10 cm`:`160-170 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2)) %>%
      select(., ID = id, UD, LD, value = BD)

# join surf raw data to replace incorrect top of splines
spline_July_BD_10cm <- left_join(spline_July_BD_10cm, July_BD_surf, by = "ID") %>%
             mutate(., value = if_else(UD == 0, VALUE, value)) %>%
             select(., -VALUE)

# join source column back on
Source <- group_by(July_BD, ID, Source) %>% summarise()
spline_July_BD_10cm <- left_join(spline_July_BD_10cm, Source, by = "ID")

write.csv(spline_July_BD_10cm, "./_field_data/spline_July_BD_10cm.csv", row.names=F)

# combine this with Mackay site BD data
spline_July_all_BD_10cm <- rbind(BD_MW_all_10cm, spline_July_BD_10cm)

write.csv(spline_July_all_BD_10cm, "./_field_data/spline_July_all_BD_10cm.csv", row.names=F)

##############################
### Spline data for APSIM parameterisation
spline_out <- ea_spline(July_BD, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,50,70,90,120,150,170)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- spline_out[["harmonised"]]

spline_July_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`150-170 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2)) %>%
      select(., ID = id, UD, LD, value = BD)

# join surf raw data to replace incorrect top of splines
spline_July_BD <- left_join(spline_July_BD, July_BD_surf, by = "ID") %>%
             mutate(., value = if_else(UD == 0, VALUE, value)) %>%
             select(., -VALUE)

# join source column back on
Source <- group_by(July_BD, ID, Source) %>% summarise()
spline_July_BD <- left_join(spline_July_BD, Source, by = "ID")

write.csv(spline_July_BD, "./_field_data/spline_July_BD.csv", row.names=F)

# combine this with Mackay site BD data
spline_July_all_BD <- rbind(select(BD_MW_all, ID, UD, LD, value, Source), spline_July_BD)

write.csv(spline_July_all_BD, "./_field_data/spline_July_all_BD.csv", row.names=F)

##############################
### Spline data for HOWLEAKY parameterisation
spline_out <- ea_spline(July_BD, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,60,90,130,150,170)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- spline_out[["harmonised"]]

spline_July_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`150-170 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2)) %>%
      select(., ID = id, UD, LD, value = BD)

# join surf raw data to replace incorrect top of splines
spline_July_BD <- left_join(spline_July_BD, July_BD_surf, by = "ID") %>%
             mutate(., value = if_else(UD == 0, VALUE, value)) %>%
             select(., -VALUE)

# join source column back on
Source <- group_by(July_BD, ID, Source) %>% summarise()
spline_July_BD <- left_join(spline_July_BD, Source, by = "ID")

write.csv(spline_July_BD, "./_field_data/spline_July_BD_HL.csv", row.names=F)

# combine this with Jenny's BD data
spline_July_all_BD <- rbind(select(Jenny_BD_sit, ID, UD, LD, value = BD, Source), spline_July_BD)

write.csv(spline_July_all_BD, "./_field_data/spline_July_all_BD_HL.csv", row.names=F)

```

# Weijin Wang's data in WT

Weijin has BD data for the Hamleigh and Toobanna soils. I don't have a SALI site number, but I will just link these to the SPC level parameters at the end of script 4b.

Here I will spline them to get consistent depths for APSIM.

```{r}

weijin <- read.csv("./_field_data/weijin_data.csv", header=T) %>% rename(., ID = soil_gp, VALUE = BD)

## put aside uppermost raw values to replace the incorrect 0-10 values created by the spline
weijin_surf <- group_by(weijin, ID) %>%
              slice(which.min(UD)) %>%
              ungroup() %>%
              select(., ID, VALUE)

####################
### Spline data for APSIM parameterisation
spline_out <- ea_spline(weijin, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,50,70,90,120)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- spline_out[["harmonised"]]

weijin_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`90-120 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2)) %>%
      select(., ID = id, UD, LD, value = BD)

# join surf raw data to replace incorrect top of splines
weijin_splined <- left_join(weijin_BD, weijin_surf, by = "ID") %>%
             mutate(., value = if_else(UD == 0, VALUE, value)) %>%
             select(., -VALUE)

write.csv(weijin_splined, "./_field_data/weijin_splined.csv", row.names=F)

####################
### Spline data for HOWLEAKY parameterisation
spline_out <- ea_spline(weijin, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,60,90,130)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- spline_out[["harmonised"]]

weijin_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`90-130 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2)) %>%
      select(., ID = id, UD, LD, value = BD)

# join surf raw data to replace incorrect top of splines
weijin_splined <- left_join(weijin_BD, weijin_surf, by = "ID") %>%
             mutate(., value = if_else(UD == 0, VALUE, value)) %>%
             select(., -VALUE)

write.csv(weijin_splined, "./_field_data/weijin_splined_HL.csv", row.names=F)

```

# Verburg et al data for Bundy

As above, I have data from Kirsten Verburg's report. It is related to SALI sites, but also just to a variety of SPCs, so here I'm going to spline it and attribute directly to my soils. 

```{r}

## Sept 2022 - add in Verburg Bundy sites
verburg <- read.csv("./_field_data/verburg_BD_data.csv", header=T) %>% rename(., ID = soil_gp, VALUE = BD)

## put aside uppermost raw values to replace the incorrect 0-10 values created by the spline
verburg_surf <- group_by(verburg, ID) %>%
              slice(which.min(UD)) %>%
              ungroup() %>%
              select(., ID, VALUE)

### Spline data for APSIM parameterisation
spline_out <- ea_spline(verburg, var.name = "VALUE", lam = 0.1, d = t(c(0,10,30,50,70,90,120,150)), vlow = 0, vhigh = Inf, show.progress=TRUE)
    
harmondat <- spline_out[["harmonised"]]

verburg_BD <- pivot_longer(harmondat, cols = `0-10 cm`:`120-150 cm`, names_to = "depth", values_to = "BD") %>%
      separate(., depth, c("UD", "LD"), "-") %>%
      mutate(., UD = as.numeric(UD), LD = as.numeric(gsub(" cm", "", LD)), BD = round(if_else(UD == 0, 1, BD),2)) %>%
      select(., ID = id, UD, LD, value = BD)

# join surf raw data to replace incorrect top of splines
verburg_splined <- left_join(verburg_BD, verburg_surf, by = "ID") %>%
             mutate(., value = if_else(UD == 0, VALUE, value)) %>%
             select(., -VALUE)

write.csv(verburg_splined, "./_field_data/verburg_splined.csv", row.names=F)


```

