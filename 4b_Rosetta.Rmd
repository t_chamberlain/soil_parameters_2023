---
title: "Rosetta"
author: "Tessa Chamberlain"
date: "02/08/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}

# run these commands in the R console
install.packages('httr', dependencies =  TRUE)
install.packages('jsonlite', dependencies =  TRUE)
install.packages('latticeExtra', dependencies = TRUE)
install.packages('tactile', dependencies =  TRUE)
install.packages('soilDB', dependencies = TRUE)
install.packages('aqp', dependencies = TRUE)

install.packages('remotes', dep=TRUE)
remotes::install_github("ncss-tech/soilDB", dependencies = FALSE, upgrade_dependencies=FALSE)

```
```{r}

library(soilDB)
library(httr)
library(jsonlite)

# import dominant SPC parameters
dd <- read.csv("./4_APSIM_parameters/dd_CYWT.csv", header=T) %>%
      select(., P50_2000, P2_50, P2, BDbest, m3rd_bar, m15_bar)

write.csv(dd, "./4_APSIM_parameters/temp.csv", row.names=F)

# set variable names
vars <- c('P50_2000', 'P2_50', 'P2', 'BDbest', 'm3rd_bar', 'm15_bar')

# run Rosetta
ros <- ROSETTA(dd, vars = vars)

```

