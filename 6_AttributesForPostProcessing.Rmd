---
title: "Soil parameter framework 2021 - Step 6 - Collating attributes for APSIM post-processing"
author: "Tessa Chamberlain"
date: "18/02/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(DBI)
library(ROracle)

rm(list=ls(all=TRUE))

```

``` {r Oracle connections, echo=FALSE, eval=TRUE}

drv    <- dbDriver('Oracle')

# SALI Connection
SALI <- dbConnect(drv,
                  username = 'chamberlaint',
                  password = 'Tuesday22',
                  dbname = 'saliprod',
                  host = 'lasun412',
                  port = 1521)

```

This script collates soil attributes that are required for post-processing steps after APSIM sims have been run. The attributes required are:

- surface total N - calculating particulate N in runoff
- surface total P - calculating particulate P in runoff
- Colwell P - for P model
- surface clay % - calculating PBI for dissolved P model. Also sets fine fraction for sediment delivery to Source catchments
- surface fines (clay+silt+fine sand %) - may contribute to sediment delivery in Source catchments (not sure if currently used, but include in case)
- K factor - used to back calculate correct erosion rate in RUSLE


In the past, these have been represented at the polygon level by ASRIS. We will now be generating them at the SPC level instead, as we feel this is sensible and more stable as many of these attributes have a patchy occurrence.

Also required is slope, to back calculate correct erosion rate in RUSLE. This will be calculated on a polygon level (mean zonal statistic).

Surface particle sizes and K factor have already been defined in Step 2 SPC attributes script for the cluster models. Here I will calculate SPC level values for the nutrient attributes.

## Bring in SPC site list

```{r}

spc_sites_final <- read.csv("./2_spc_attributes/spc_sites_final.csv", header=T) %>% select(., -X)

```


## Surface TN

```{r}

TN_sit_raw <- dbGetQuery(SALI, "select
                            l.project_code||'-'||l.site_id||'-'||l.obs_no as ID,
                            l.lab_meth_code,
                            l.numeric_value,
                            l.result_status
                            from
                            sit_lab_results l
                            left join
                            sit_samples s
                            on
                            l.project_code = s.project_code
                            and
                            l.site_id = s.site_id
                            and
                            l.obs_no = s.obs_no
                            and
                            l.horizon_no = s.horizon_no
                            and
                            l.sample_no = s.sample_no
                            where
                            l.lab_meth_code in ('7A2', '7A5', '7A1', '7_NR')
                            and
                            s.upper_depth = 0
                            ") %>%
         filter(., !is.na(NUMERIC_VALUE)) %>%
         inner_join(., spc_sites_final, by = "ID")

# review different lab method codes
TN_LabMeths <- group_by(TN_sit_raw, LAB_METH_CODE) %>% summarise(., count=n())

# check disturbance categories for sites
# 16% don't have disturbance recorded, 21% are cultivated
TN_sit_raw_disturb <- group_by(TN_sit_raw, DISTURB_TYPE) %>% summarise(., count = n(), pct = round(count/nrow(TN_sit_raw)*100))

# filter only cultivated data
TN_sit_raw_cult <- filter(TN_sit_raw, DISTURB_TYPE %in% c("6", "7"))
write.csv(TN_sit_raw_cult, "./6_APSIM_post-processing/TN_sit_raw_cult.csv")

# median multiple site results across NAME
TNcult_SPC <- group_by(TN_sit_raw_cult, NAME) %>% 
          summarise(., TNcult = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# median all sites results across NAME (including uncultivated)
TNall_SPC <- group_by(TN_sit_raw, NAME) %>%
                summarise(., TNall = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# split by cluster region, and compare to clusters
SPC_clusters_MW <- read.csv("./3_cluster_models/SPC_atts_MW_clust_mer.csv", header=T) %>% select(., NAME, Area_ha, clusters_MW, soil_gp)

TN_SPC_MW <- left_join(SPC_clusters_MW, TNcult_SPC, by = "NAME") %>%
             left_join(., TNall_SPC, by = "NAME") %>%
             arrange(., soil_gp, desc(Area_ha))

```

## Surface TP

```{r}

TP_sit_raw <- dbGetQuery(SALI, "select
                            l.project_code||'-'||l.site_id||'-'||l.obs_no as ID,
                            l.lab_meth_code,
                            l.numeric_value,
                            l.result_status
                            from
                            sit_lab_results l
                            left join
                            sit_samples s
                            on
                            l.project_code = s.project_code
                            and
                            l.site_id = s.site_id
                            and
                            l.obs_no = s.obs_no
                            and
                            l.horizon_no = s.horizon_no
                            and
                            l.sample_no = s.sample_no
                            where
                            l.lab_meth_code in ('9A1', '9A3a')
                            and
                            s.upper_depth = 0
                            ") %>%
         filter(., !is.na(NUMERIC_VALUE)) %>%
         inner_join(., spc_sites_final, by = "ID")

# review different lab method codes
TP_LabMeths <- group_by(TP_sit_raw, LAB_METH_CODE) %>% summarise(., count=n())

# check disturbance categories for sites
# 17% don't have disturbance recorded, 23% are cultivated
TP_sit_raw_disturb <- group_by(TP_sit_raw, DISTURB_TYPE) %>% summarise(., count = n(), pct = round(count/nrow(TP_sit_raw)*100))

# filter only cultivated data
TP_sit_raw_cult <- filter(TP_sit_raw, DISTURB_TYPE %in% c("6", "7"))
write.csv(TP_sit_raw_cult, "./6_APSIM_post-processing/TP_sit_raw_cult.csv")

# median multiple site results across NAME (cultivated)
TPcult_SPC <- group_by(TP_sit_raw_cult, NAME) %>% 
          summarise(., TPcult = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# median all sites results across NAME (including uncultivated)
TPall_SPC <- group_by(TP_sit_raw, NAME) %>%
                summarise(., TPall = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# split by cluster region, and compare to clusters
SPC_clusters_MW <- read.csv("./3_cluster_models/SPC_atts_MW_clust_mer.csv", header=T) %>% select(., NAME, Area_ha, clusters_MW, soil_gp)

TP_SPC_MW <- left_join(SPC_clusters_MW, TPcult_SPC, by = "NAME") %>%
             left_join(., TPall_SPC, by = "NAME") %>%
             arrange(., soil_gp, desc(Area_ha))

```

## Colwell P

```{r}

CollP_sit_raw <- dbGetQuery(SALI, "select
                            l.project_code||'-'||l.site_id||'-'||l.obs_no as ID,
                            l.lab_meth_code,
                            l.numeric_value,
                            l.result_status
                            from
                            sit_lab_results l
                            left join
                            sit_samples s
                            on
                            l.project_code = s.project_code
                            and
                            l.site_id = s.site_id
                            and
                            l.obs_no = s.obs_no
                            and
                            l.horizon_no = s.horizon_no
                            and
                            l.sample_no = s.sample_no
                            where
                            l.lab_meth_code in ('9B1', '9B2')
                            and
                            s.upper_depth = 0
                            ") %>%
         filter(., !is.na(NUMERIC_VALUE)) %>%
         inner_join(., spc_sites_final, by = "ID")

# review different lab method codes
CollP_LabMeths <- group_by(CollP_sit_raw, LAB_METH_CODE) %>% summarise(., count=n())

# check disturbance categories for sites
# 17% don't have disturbance recorded, 19% are cultivated
CollP_sit_raw_disturb <- group_by(CollP_sit_raw, DISTURB_TYPE) %>% summarise(., count = n(), pct = round(count/nrow(CollP_sit_raw)*100))

# filter only cultivated data
CollP_sit_raw_cult <- filter(CollP_sit_raw, DISTURB_TYPE %in% c("6", "7"))
write.csv(CollP_sit_raw_cult, "./6_APSIM_post-processing/CollP_sit_raw_cult.csv")

# median multiple site results across NAME
CollPcult_SPC <- group_by(CollP_sit_raw_cult, NAME) %>% 
             summarise(., CollPcult = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# median all sites results across NAME (including uncultivated)
CollPall_SPC <- group_by(CollP_sit_raw, NAME) %>%
                summarise(., CollPall = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# split by cluster region, and compare to clusters
SPC_clusters_MW <- read.csv("./3_cluster_models/SPC_atts_MW_clust_mer.csv", header=T) %>% select(., NAME, Area_ha, clusters_MW, soil_gp)

CollP_SPC_MW <- left_join(SPC_clusters_MW, CollPcult_SPC, by = "NAME") %>%
             left_join(., CollPall_SPC, by = "NAME") %>%
             arrange(., soil_gp, desc(Area_ha))

CollP_SPC_MW_domSoils <- filter(CollP_SPC_MW, NAME %in% c("Calen", "Cameron", "Dunwold", "Mirani", "Pindi", "Proserpine", "Sandiford", "St Helens", "Tedlands", "Victoria Plains")) %>%
                         select(., -c("Area_ha", "clusters_MW", "soil_gp"))

```

## Phosphorus buffer index

```{r}

PBI_sit_raw <- dbGetQuery(SALI, "select
                            l.project_code||'-'||l.site_id||'-'||l.obs_no as ID,
                            l.lab_meth_code,
                            l.numeric_value,
                            l.result_status
                            from
                            sit_lab_results l
                            left join
                            sit_samples s
                            on
                            l.project_code = s.project_code
                            and
                            l.site_id = s.site_id
                            and
                            l.obs_no = s.obs_no
                            and
                            l.horizon_no = s.horizon_no
                            and
                            l.sample_no = s.sample_no
                            where
                            l.lab_meth_code in ('9I2', '9I2b', '9I3', '9I4', '9I4b')
                            and
                            s.upper_depth = 0
                            ") %>%
         filter(., !is.na(NUMERIC_VALUE)) %>%
         inner_join(., spc_sites_final, by = "ID")

# review different lab method codes
PBI_LabMeths <- group_by(PBI_sit_raw, LAB_METH_CODE) %>% summarise(., count=n())

# check disturbance categories for sites
# 2% don't have disturbance recorded, 72% are cultivated
PBI_sit_raw_disturb <- group_by(PBI_sit_raw, DISTURB_TYPE) %>% summarise(., count = n(), pct = round(count/nrow(PBI_sit_raw)*100))

# filter only cultivated data
PBI_sit_raw_cult <- filter(PBI_sit_raw, DISTURB_TYPE %in% c("6", "7"))
write.csv(PBI_sit_raw_cult, "./6_APSIM_post-processing/PBI_sit_raw_cult.csv")

# median multiple site results across NAME
PBIcult_SPC <- group_by(PBI_sit_raw_cult, NAME) %>% 
             summarise(., PBIcult = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# median all sites results across NAME (including uncultivated)
PBIall_SPC <- group_by(PBI_sit_raw, NAME) %>%
                summarise(., PBIall = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# split by cluster region, and compare to clusters
SPC_clusters_MW <- read.csv("./3_cluster_models/SPC_atts_MW_clust_mer.csv", header=T) %>% select(., NAME, Area_ha, clusters_MW, soil_gp)

PBI_SPC_MW <- left_join(SPC_clusters_MW, PBIcult_SPC, by = "NAME") %>%
             left_join(., PBIall_SPC, by = "NAME") %>%
             arrange(., soil_gp, desc(Area_ha))

PBI_SPC_MW_domSoils <- filter(PBI_SPC_MW, NAME %in% c("Calen", "Cameron", "Dunwold", "Mirani", "Pindi", "Proserpine", "Sandiford", "St Helens", "Tedlands", "Victoria Plains")) %>%
                       select(., -c("Area_ha", "clusters_MW", "soil_gp"))

```

## Effective CEC

```{r}

ECEC_sit_raw <- dbGetQuery(SALI, "select
                            l.project_code||'-'||l.site_id||'-'||l.obs_no as ID,
                            l.lab_meth_code,
                            l.numeric_value,
                            l.result_status
                            from
                            sit_lab_results l
                            left join
                            sit_samples s
                            on
                            l.project_code = s.project_code
                            and
                            l.site_id = s.site_id
                            and
                            l.obs_no = s.obs_no
                            and
                            l.horizon_no = s.horizon_no
                            and
                            l.sample_no = s.sample_no
                            where
                            l.lab_meth_code in ('15J1')
                            and
                            s.upper_depth = 0
                            ") %>%
         filter(., !is.na(NUMERIC_VALUE)) %>%
         inner_join(., spc_sites_final, by = "ID")

# review different lab method codes
ECEC_LabMeths <- group_by(ECEC_sit_raw, LAB_METH_CODE) %>% summarise(., count=n())

# check disturbance categories for sites
# 2% don't have disturbance recorded, 72% are cultivated
PBI_sit_raw_disturb <- group_by(PBI_sit_raw, DISTURB_TYPE) %>% summarise(., count = n(), pct = round(count/nrow(PBI_sit_raw)*100))

# filter only cultivated data
PBI_sit_raw_cult <- filter(PBI_sit_raw, DISTURB_TYPE %in% c("6", "7"))
write.csv(PBI_sit_raw_cult, "./6_APSIM_post-processing/PBI_sit_raw_cult.csv")

# median multiple site results across NAME
PBIcult_SPC <- group_by(PBI_sit_raw_cult, NAME) %>% 
             summarise(., PBIcult = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# median all sites results across NAME (including uncultivated)
PBIall_SPC <- group_by(PBI_sit_raw, NAME) %>%
                summarise(., PBIall = median(NUMERIC_VALUE, na.rm=T), count_sites = n())

# split by cluster region, and compare to clusters
SPC_clusters_MW <- read.csv("./3_cluster_models/SPC_atts_MW_clust_mer.csv", header=T) %>% select(., NAME, Area_ha, clusters_MW, soil_gp)

PBI_SPC_MW <- left_join(SPC_clusters_MW, PBIcult_SPC, by = "NAME") %>%
             left_join(., PBIall_SPC, by = "NAME") %>%
             arrange(., soil_gp, desc(Area_ha))

PBI_SPC_MW_domSoils <- filter(PBI_SPC_MW, NAME %in% c("Calen", "Cameron", "Dunwold", "Mirani", "Pindi", "Proserpine", "Sandiford", "St Helens", "Tedlands", "Victoria Plains")) %>%
                       select(., -c("Area_ha", "clusters_MW", "soil_gp"))

```
