# README #

This repository contains code used to develop a new soil parameter framework to support APSIM and Howleaky modelling for Reef Report Cards in 2021 and beyond. It covers areas of grain cropping, bananas, sugarcane and horticulture.